.. DTW documentation master file, created by
   sphinx-quickstart on Mon Jan 30 15:59:03 2023.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to DTW's documentation!
===============================

DTW is a dynamic time warping library designed for tokamak velocimetry
measurements.

The algorithm works by distorting image 2 into image 1, then
using the distorted coordinates to compute the displacements required
to distort image 1 into image 2, given at the positions in image 1.


.. toctree::
   :maxdepth: 2
   :caption: Contents:

   api/modules


Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
