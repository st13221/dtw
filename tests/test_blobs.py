import numpy as np

import dtw


import pytest


def gaussian(x, x0: float, sigma: float):
    """Create a Gaussian"""
    return np.exp(-((x - x0) ** 2) / (2 * sigma**2))


def blob_maker(A, X, Y, x0: float, y0: float, sigma_x: float, sigma_y: float):
    """Create a 2D elliptic Gaussian"""
    return A * gaussian(X, x0, sigma_x) * gaussian(Y, y0, sigma_y)


def test_three_blobs():
    x = np.linspace(1, 128, 128)
    y = np.linspace(1, 64, 64)
    xx, yy = np.meshgrid(x, y)

    B1 = blob_maker(32, xx, yy, 32.0, 48.0, 12.0, 7.0)
    B2 = blob_maker(24.0, xx, yy, 97, 33.0, 9.0, 6.0)
    B3 = blob_maker(29.0, xx, yy, 79, 15.0, 6.0, 3.5)
    B1A = blob_maker(32.0, xx, yy, 38.0, 41.0, 12.0, 7.0)
    B2A = blob_maker(24.0, xx, yy, 103.0, 26.0, 9.0, 6.0)
    B3A = blob_maker(29.0, xx, yy, 85.0, 9.0, 6.0, 3.5)
    z1 = B1 + B2 + B3
    z2 = B1A + B2A + B3A

    vel_x, vel_y = dtw.DTW(z1, z2, xx, yy)

    tol = 1e-2
    assert np.isclose(vel_x[47, 31], 5.46, rtol=tol)
    assert np.isclose(vel_y[47, 31], -7.01, rtol=tol)

    assert np.isclose(vel_x[32, 96], 5.66, rtol=tol)
    assert np.isclose(vel_y[32, 96], -7.85, rtol=tol)

    assert np.isclose(vel_x[14, 78], 5.26, rtol=tol)
    assert np.isclose(vel_y[14, 78], -5.61, rtol=tol)


@pytest.mark.parametrize(
    ("x_displacement", "y_displacement"), [(-4, 0), (4, 0), (0, 4), (0, -4)]
)
def test_single_blob(x_displacement, y_displacement):
    NX = 32
    NY = 32

    x = np.linspace(1, NX, NX)
    y = np.linspace(1, NY, NY)
    xx, yy = np.meshgrid(x, y)

    A = 86
    x0 = NX // 2
    y0 = NY // 2
    width = 8
    blob1 = blob_maker(A, xx, yy, x0, y0, width, width)
    blob2 = blob_maker(
        A, xx, yy, x0 + x_displacement, y0 + y_displacement, width, width
    )

    vel_x, vel_y = dtw.DTW(blob1, blob2, xx, yy, x_first=(x_displacement != 0))

    mask = blob1 >= (A / 2)
    masked_vel_x = vel_x[mask]
    masked_vel_y = vel_y[mask]

    assert np.isclose(np.mean(masked_vel_x), x_displacement, rtol=1e-1, atol=1e-1)
    assert np.isclose(np.mean(masked_vel_y), y_displacement, rtol=1e-1, atol=1e-1)
