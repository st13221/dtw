# -*- coding: utf-8 -*-
# vim:fenc=utf-8
#
# Copyright © 2020 Edward Higgins <ed.higgins@york.ac.uk>
#
# Distributed under terms of the MIT license.

import numpy as np
from numpy.testing import assert_allclose
import sys

from dtw import dtw_f, boxcar, local_distance_new, global_distance_new, all_iterations, displacement_maker

np.set_printoptions(threshold=sys.maxsize)
def test_abs_diff():
    A = np.random.rand(10)*10
    assert_allclose(abs(np.diff(A)), dtw_f.abs_diff(A))

    A = np.random.rand(1)*10
    assert_allclose(abs(np.diff(A)), dtw_f.abs_diff(A))

    A = np.zeros(10)
    assert_allclose(abs(np.diff(A)), dtw_f.abs_diff(A))

def test_mean():
    A = np.random.rand(10)*10
    assert_allclose(np.mean(A), dtw_f.mean(A, 1, 10))

    assert_allclose(np.mean(A[0:5]), dtw_f.mean(A, 1, 5))

    assert_allclose(np.mean(A[5:]), dtw_f.mean(A, 6, 10))

def test_boxcar():
    # Random numbers
    displacement = np.array([8.81181151, 3.95109714, 6.41161932, 2.05868299,
        8.8546759 , 5.89688628, 4.08827453, 0.12149912, 8.23392819, 7.09123416,
        6.83909171, 4.44205796, 7.51198148, 0.85122267, 4.57251309, 4.16565432,
        2.25843149, 5.56489665, 9.70307313, 5.47270531, 2.75413953, 2.70609566,
        2.71320664, 1.51035396, 3.29998841, 3.81114167, 0.65853308, 2.52567561,
        2.36574573, 7.9105903 , 4.16436366, 2.13197977])
    m = 3
    l = 3.0
    new_displacement = np.array([8.81181151, 7.92015925, 7.44930525, 7.10660528,
        6.81903699, 6.55805886, 6.32864801, 6.1231555 , 5.84194008, 5.6255389 ,
        5.43548106, 5.26083658, 5.09771939, 4.94431247, 4.79839717, 4.65851608,
        4.53092393, 4.40643522, 4.28645261, 4.16983645, 4.05542156, 3.94173452,
        3.8274434 , 3.70717524, 3.5505556 , 3.43272638, 3.31028667, 3.17797808,
        3.03469366, 2.87077495, 2.64265976, 2.13197977])
    assert_allclose(boxcar(displacement, m, l), new_displacement)

    # Check optional l is working fine
    assert_allclose(boxcar(displacement, m), new_displacement)

    # Random noise
    displacement = np.ones(32) + np.random.rand(32)
    new_displacement = np.array(displacement, copy=True)
    m=3
    assert_allclose(boxcar(displacement,m), new_displacement)

    # zeros
    displacement = np.zeros(32)
    new_displacement = np.zeros(32)
    assert_allclose(boxcar(displacement, m, l), new_displacement)

    # odd sized displacement array
    displacement = np.array([2.22874012, 8.65231763, 1.29467402, 5.28132137,
        5.61755598, 9.60014296, 0.29871141, 1.45962126, 3.44486969, 0.89574927,
        9.09394743, 7.6841595 , 4.32176085, 3.83645857, 8.46340082, 6.44517435,
        9.75937483, 0.89636618, 7.98777464, 9.67129983, 2.22460371, 0.63095777,
        8.64013503, 7.68762867, 9.92634907, 3.52210655, 1.38891684, 8.17441585,
        7.73168005, 3.27350529, 7.43795983])
    new_displacement = np.array([2.22874012, 3.05057551, 3.42965283,
        3.69378121, 3.91455205, 4.11110834, 4.28104298, 4.43063376, 4.65079493,
        4.80640389, 4.94187388, 5.06472713, 5.17823992, 5.2834132 , 5.38202982,
        5.47491978, 5.5608008 , 5.64152042, 5.7186268 , 5.79343532, 5.86653406,
        5.94100975, 6.02550954, 6.15262968, 6.23079035, 6.31438603, 6.41035484,
        6.51958985, 6.65570313, 6.87617987, 7.43795983])

    assert_allclose(boxcar(displacement, m, l), new_displacement)

def test_local_distance_new():
    # Random arrays
    A = np.array([
        [0.59387485, 0.47629528, 0.04807315, 0.1488401 , 0.84202328],
        [0.4026589 , 0.43221964, 0.1235364 , 0.64591577, 0.85076651],
        [0.92912483, 0.29651212, 0.40286947, 0.87904906, 0.72869379],
        [0.3633322 , 0.23130696, 0.24275265, 0.03655791, 0.35587686],
        [0.58410965, 0.77596404, 0.1655205 , 0.55978776, 0.47693965],
        [0.95178971, 0.83697901, 0.92367722, 0.78190081, 0.05178331]
    ])
    B = np.array([
        [0.30388894, 0.76989242, 0.965846  , 0.45001351, 0.67132136, 0.63007297, 0.96866977],
        [0.35208064, 0.79871575, 0.16846784, 0.5565932 , 0.54923144, 0.20074702, 0.76844406],
        [0.35593565, 0.28224123, 0.27943929, 0.65211765, 0.19875444, 0.37317418, 0.6148921 ],
        [0.86657473, 0.60835944, 0.4316966 , 0.52006888, 0.01895297, 0.63734959, 0.58958132],
        [0.56298744, 0.98860197, 0.22670693, 0.66773005, 0.10380947, 0.31370799, 0.32248758],
        [0.0235922 , 0.95072601, 0.59584715, 0.97555341, 0.27035325, 0.92694484, 0.70513155]
    ])
    d_actual = np.array([
        [0.        ,     np.inf,     np.inf,     np.inf,     np.inf,     np.inf,     np.inf,     np.inf],
        [    np.inf, 1.99682707, 2.16664775, 1.70774582, 0.94879628, 2.37739872, 1.82778692, 1.41126859],
        [    np.inf, 1.45924628, 1.10808242, 0.95516867, 1.32639952, 1.10629784, 1.35257571, 1.76974908],
        [    np.inf, 1.64597189, 1.91488019, 0.63841575, 1.5988171 , 1.1108943 , 0.9232981 , 1.56505331],
        [    np.inf, 2.65308157, 2.51591971, 2.35957847, 1.42150899, 1.64439263, 2.47961957, 1.72692852],
        [    np.inf, 2.00222794, 1.6539017 , 1.59423157, 0.77065537, 2.03435255, 1.71427267, 0.79225169]
    ])
    lenA_actual = 5
    lenB_actual = 7
    d,lenA,lenB = local_distance_new(A,B,'cosine')
    assert lenA == lenA_actual
    assert lenB == lenB_actual
    assert_allclose(d,d_actual)

    # zeros
    A = np.zeros([10,10])
    B = np.zeros([10,10])
    d_actual = np.array([
        [    0., np.inf, np.inf, np.inf, np.inf, np.inf, np.inf, np.inf, np.inf, np.inf, np.inf],
        [np.inf,     0.,     0.,     0.,     0.,     0.,     0.,     0.,     0.,     0.,     0.],
        [np.inf,     0.,     0.,     0.,     0.,     0.,     0.,     0.,     0.,     0.,     0.],
        [np.inf,     0.,     0.,     0.,     0.,     0.,     0.,     0.,     0.,     0.,     0.],
        [np.inf,     0.,     0.,     0.,     0.,     0.,     0.,     0.,     0.,     0.,     0.],
        [np.inf,     0.,     0.,     0.,     0.,     0.,     0.,     0.,     0.,     0.,     0.],
        [np.inf,     0.,     0.,     0.,     0.,     0.,     0.,     0.,     0.,     0.,     0.],
        [np.inf,     0.,     0.,     0.,     0.,     0.,     0.,     0.,     0.,     0.,     0.],
        [np.inf,     0.,     0.,     0.,     0.,     0.,     0.,     0.,     0.,     0.,     0.],
        [np.inf,     0.,     0.,     0.,     0.,     0.,     0.,     0.,     0.,     0.,     0.],
        [np.inf,     0.,     0.,     0.,     0.,     0.,     0.,     0.,     0.,     0.,     0.]
    ])
    lenA_actual = 10
    lenB_actual = 10
    d,lenA,lenB = local_distance_new(A,B,'cosine')
    assert lenA == lenA_actual
    assert lenB == lenB_actual
    assert_allclose(d,d_actual)

def test_global_distance():
    # Result from local_distance_new random test
    d = np.array([
        [0.        ,     np.inf,     np.inf,     np.inf,     np.inf,     np.inf,     np.inf,     np.inf],
        [    np.inf, 1.99682707, 2.16664775, 1.70774582, 0.94879628, 2.37739872, 1.82778692, 1.41126859],
        [    np.inf, 1.45924628, 1.10808242, 0.95516867, 1.32639952, 1.10629784, 1.35257571, 1.76974908],
        [    np.inf, 1.64597189, 1.91488019, 0.63841575, 1.5988171 , 1.1108943 , 0.9232981 , 1.56505331],
        [    np.inf, 2.65308157, 2.51591971, 2.35957847, 1.42150899, 1.64439263, 2.47961957, 1.72692852],
        [    np.inf, 2.00222794, 1.6539017 , 1.59423157, 0.77065537, 2.03435255, 1.71427267, 0.79225169]
    ])
    lenA = 5
    lenB = 7
    m = 3
    D_actual = np.array([
        [        0.,     np.inf,     np.inf,     np.inf,     np.inf,     np.inf,     np.inf,     np.inf],
        [    np.inf,     np.inf,     np.inf,     np.inf, 0.        ,     np.inf,     np.inf,     np.inf],
        [    np.inf,     np.inf,     np.inf, 0.        , 1.32639952, 2.21259568,     np.inf,     np.inf],
        [    np.inf,     np.inf, 0.        , 0.63841575, 2.23723285, 3.32348998, 4.05919188,     np.inf],
        [    np.inf, 0.        , 2.51591971, 2.99799422, 3.48143373, 4.96788261, 6.53881145, 7.51304892],
        [    np.inf,     np.inf, 3.3078034 , 4.59222579, 4.2520891 , 6.28644165, 8.00071432, 8.12331483]
    ])
    ptr_actual = np.array([[    0.,     0.,     0.,     0.,     0.,     0.,     0.,     0.],
                           [    0.,     0.,     0.,     0.,     0., np.inf, np.inf, np.inf],
                           [    0.,     0.,     0.,     0.,     2.,     0., np.inf, np.inf],
                           [    0.,     0.,     0.,     1.,     2.,     1.,     0., np.inf],
                           [    0.,     0.,     1.,     1.,     0.,     1.,     1.,     0.],
                           [    0., np.inf,     0.,     1.,     1.,     2.,     2.,     0.]])
    D, pointer = global_distance_new(d, lenA, lenB, m)
    assert_allclose(D, D_actual)
    assert_allclose(pointer, ptr_actual)

    # Result from local_distance_new zero test
    d = np.array([
        [    0., np.inf, np.inf, np.inf, np.inf, np.inf, np.inf, np.inf, np.inf, np.inf, np.inf],
        [np.inf,     0.,     0.,     0.,     0.,     0.,     0.,     0.,     0.,     0.,     0.],
        [np.inf,     0.,     0.,     0.,     0.,     0.,     0.,     0.,     0.,     0.,     0.],
        [np.inf,     0.,     0.,     0.,     0.,     0.,     0.,     0.,     0.,     0.,     0.],
        [np.inf,     0.,     0.,     0.,     0.,     0.,     0.,     0.,     0.,     0.,     0.],
        [np.inf,     0.,     0.,     0.,     0.,     0.,     0.,     0.,     0.,     0.,     0.],
        [np.inf,     0.,     0.,     0.,     0.,     0.,     0.,     0.,     0.,     0.,     0.],
        [np.inf,     0.,     0.,     0.,     0.,     0.,     0.,     0.,     0.,     0.,     0.],
        [np.inf,     0.,     0.,     0.,     0.,     0.,     0.,     0.,     0.,     0.,     0.],
        [np.inf,     0.,     0.,     0.,     0.,     0.,     0.,     0.,     0.,     0.,     0.],
        [np.inf,     0.,     0.,     0.,     0.,     0.,     0.,     0.,     0.,     0.,     0.]
    ])
    lenA = 10
    lenB = 10
    m = 3
    D_actual = np.array([
        [    0., np.inf, np.inf, np.inf, np.inf, np.inf, np.inf, np.inf, np.inf, np.inf, np.inf],
        [np.inf, np.inf, np.inf, np.inf,     0., np.inf, np.inf, np.inf, np.inf, np.inf, np.inf],
        [np.inf, np.inf, np.inf,     0.,     0.,     0., np.inf, np.inf, np.inf, np.inf, np.inf],
        [np.inf, np.inf,     0.,     0.,     0.,     0.,     0., np.inf, np.inf, np.inf, np.inf],
        [np.inf,     0.,     0.,     0.,     0.,     0.,     0.,     0., np.inf, np.inf, np.inf],
        [np.inf, np.inf,     0.,     0.,     0.,     0.,     0.,     0.,     0., np.inf, np.inf],
        [np.inf, np.inf, np.inf,     0.,     0.,     0.,     0.,     0.,     0.,     0., np.inf],
        [np.inf, np.inf, np.inf, np.inf,     0.,     0.,     0.,     0.,     0.,     0.,     0.],
        [np.inf, np.inf, np.inf, np.inf, np.inf,     0.,     0.,     0.,     0.,     0.,     0.],
        [np.inf, np.inf, np.inf, np.inf, np.inf, np.inf,     0.,     0.,     0.,     0.,     0.],
        [np.inf, np.inf, np.inf, np.inf, np.inf, np.inf, np.inf,     0.,     0.,     0.,     0.]
    ])
    ptr_actual = np.array([
        [    0.,     0.,     0.,     0.,     0.,     0.,     0.,     0.,     0.,     0.,     0.],
        [    0.,     0.,     0.,     0.,     0., np.inf, np.inf, np.inf, np.inf, np.inf, np.inf],
        [    0.,     0.,     0.,     0.,     2.,     2., np.inf, np.inf, np.inf, np.inf, np.inf],
        [    0.,     0.,     0.,     1.,     2.,     2.,     2., np.inf, np.inf, np.inf, np.inf],
        [    0.,     0.,     1.,     1.,     0.,     2.,     2.,     2., np.inf, np.inf, np.inf],
        [    0., np.inf,     1.,     1.,     1.,     0.,     2.,     2.,     2., np.inf, np.inf],
        [    0., np.inf, np.inf,     1.,     1.,     1.,     0.,     2.,     2.,     2., np.inf],
        [    0., np.inf, np.inf, np.inf,     1.,     1.,     1.,     0.,     2.,     2.,     2.],
        [    0., np.inf, np.inf, np.inf, np.inf,     1.,     1.,     1.,     0.,     2.,     2.],
        [    0., np.inf, np.inf, np.inf, np.inf, np.inf,     1.,     1.,     1.,     0.,     2.],
        [    0., np.inf, np.inf, np.inf, np.inf, np.inf, np.inf,     1.,     1.,     1.,     0.]
    ])
    D, pointer = global_distance_new(d, lenA, lenB, m)
    assert_allclose(D, D_actual)
    assert_allclose(pointer, ptr_actual)

def test_examples():
    data = np.load('examples/full_test_01.npz')
    m_list = data['m_list']
    strip_width_list = data['strip_width_list']
    x = data['x']
    y = data['y']
    xx,yy = np.meshgrid(x,y)
    new_z1, oxx, oyy = all_iterations(data['z1'], data['z2'], xx, yy, \
                    strip_width_list, m_list)
    vel_x, vel_y = displacement_maker(xx, yy, oxx, oyy)
    assert_allclose(vel_x, data['vx1'])
    assert_allclose(vel_y, data['vy1'])
