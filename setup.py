from numpy.distutils.core import Extension, setup

dtw_f = Extension(name="dtw_f", sources=["dtw/dtw_f.f90"])

setup(ext_modules=[dtw_f])
