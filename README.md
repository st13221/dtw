# DTW v1.0.2

DTW is a dynamic time warping library designed for tokamak velocimetry
measurements.

The algorithm works by distorting image 2 into image 1, then
using the distorted coordinates to compute the displacements required
to distort image 1 into image 2, given at the positions in image 1.

## Referencing

If you use this library in your work, please cite it using the
CITATION.cff or citation.bib file in the main repository.

## Installation

DTW includes some compiled Fortran code, so you must have a working
Fortran compiler on your system, such as `gfortran`.

To build and install DTW, first clone this repo:

```shell
$ git clone https://gitlab.com/st13221/dtw.git
$ cd dtw
```

You can then build and install it using `pip`:

```shell
$ pip3 install .
```

This installs the `dtw` package into either your user's site packages,
or into a virtual environment if you're using one.

## Usage

Given two images, `image_1`, `image_2` as 2D numpy arrays with
2D coordinate arrays `x_coords`, `y_coords` (for example, as given by
the output of `numpy.meshgrid`), the velocity components for each
array element in units of pixels per frame can be computed with:

```python
import dtw

vel_x, vel_y = dtw.DTW(image_1, image_2, x_coords, y_coords)
```

## Acknowledgement

The authors wish to thank: Dr Edward Higgins for helping with the
initial `f2py` port of some of the routine's functions; Yorick Enters
and Matthew Hill for their work on initial testing of the routine and
providing feedback that lead to improvements in the library; and Dr.
Peter Hill of the PlasmaFair project for assisting in making this
library widely available for use by other researchers. This work was
supported by the Engineering and Physical Sciences Research Council
(Nos. EP/R034737/1, EP/R512230/1, EP/V051822/1, and EP/L01663X/1).
