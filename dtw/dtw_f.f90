module dtw_f
!==============================================================================#
! DTW_F
!------------------------------------------------------------------------------#
! Author:  Edward Higgins <ed.higgins@york.ac.uk>
!------------------------------------------------------------------------------#
! Version: 0.1.1, 2020-05-19
!------------------------------------------------------------------------------#
! This code is distributed under the MIT license.
!==============================================================================#
    use ieee_arithmetic
    implicit none

    public

    real(8), parameter :: pi = 4*atan(1d0)

contains
    function inf()
        real(8) :: inf
        inf = ieee_value(inf, ieee_positive_inf)
    end function inf

    function abs_diff(A)
        real(8), intent(in)  :: A(:)
        real(8) :: abs_diff(size(A)-1)
        integer :: i

        do i=1, size(A)-1
            abs_diff(i) = abs(A(i+1) - A(i))
        end do
    end function abs_diff

    function mean(A, start, finish)
        real(8), intent(in) :: A(:)
        integer, intent(in) :: start
        integer, intent(in) :: finish
        real(8) :: mean

        mean = sum(A(start:finish)) / (1+finish-start)

    end function mean

    subroutine boxcar(displacement, m, new_displacement, l)
        ! Inputs
        real(8), intent(in) :: displacement(:)
        integer, intent(in) :: m

        ! Outputs
        real(8), intent(out) :: new_displacement(size(displacement))

        ! Optional arguments
!f2py real(8) optional, intent(in) :: l = 3
        real(8), intent(in) :: l

        ! Local variables
        integer :: window
        integer :: whole_len
        integer :: half
        integer :: i, iter

        real(8) :: tmp_displacement(size(displacement))
        real(8) :: change_in_displacement(size(displacement)-1)

        ! displacement will be the displacement array that is being smoothed, window
        ! is the size of the boxcar that I want to use
        window = 15
        whole_len = size(displacement)
        ! this is the total x or y direction

        if (mod(window, 2) == 1) window = window - 1
        ! this makes the window an odd number all the time

        half = window / 2

        ! while loop means that any time the displacement between two adjacent pixels
        ! varies too much that the boxcar is run again.
        change_in_displacement = abs_diff(displacement)
        tmp_displacement = displacement
        new_displacement = displacement

        iter = 0
        do while (any(change_in_displacement > (real(m,8) / l)))
            iter=iter+1
            ! empty array to put the smoothed displacement into
            new_displacement = 0
            ! the central half of the displacement is smoothed first
            do i=half, whole_len-half-1
                new_displacement(1+i) = mean(tmp_displacement, (1+i-half), (i+half+1))
            end do

            ! the ends are treated slightly differently
            do i=0, half-1
                new_displacement(1+i) = mean(tmp_displacement, 1, ((i*2)+1))
                new_displacement(1+i+whole_len-half) = mean(tmp_displacement,                   &
                                                            1+whole_len - (2*half) + 1 + (2*i), &
                                                            whole_len)
            end do

            ! change_in_displacement is the different between displacement of
            ! adjacent pixels
            change_in_displacement = abs_diff(new_displacement)
            tmp_displacement = new_displacement
        end do

    end subroutine boxcar

    subroutine local_distance_new(A, B, window, d, lenA, lenB)
        ! Inputs
        real(8), intent(in) :: A(:,:)
        real(8), intent(in) :: B(:,:)
        ! character(len=*), intent(in) :: alpha
        real(8) :: window(size(A,1))

        ! Outputs
        real(8), intent(out) :: d(size(A,2)+1,size(B,2)+1)
        integer, intent(out)  :: lenA
        integer, intent(out)  :: lenB

        ! Local variables
        ! integer :: width
        integer :: i, j

        ! real(8) :: p(size(A,1))


        !make the lengths of the signals variables
        lenA = size(A,2)
        lenB = size(B,2)

        ! here we assume the strips are the same width, and they should be.
        ! p is orthogonal to stripping direction
        ! here only two options are included, 'cosine', which is the alpha window
        ! function as described in quenot 1998, or it just uses an array of ones
        ! width = size(A,1)

        ! if (size(B,1) /= width) then
        !     print *, "Error in local_distance_new: A and B are different widths"
        !     error stop
        ! end if

        ! if (alpha == 'cosine') then
        !     do i = 1, width
        !         p(i) = -real(width,8)/2 + (i-1)*real(width,8)/(width-1)
        !         window(i) = 1 + cos(2*pi * p(i) / width)
        !     end do
        ! else
        !     window = 1
        ! end if

        !d = the local difference matrix with extra element of infinity for
        !the algorithm to use min correctly with
        d = 0
        d(1,2:) = inf()
        d(2:,1) = inf()

        !fills in the whole local difference matrix d
        do i=1, lenA
            do j=1, lenB
                d(i+1,j+1) = sum(window * abs(A(:,i) - B(:,j)))
            end do
        end do
        !returns the local difference matrix d, and the lengths of each signal

    end subroutine local_distance_new

    subroutine global_distance_new(d_local, lenA, lenB, m, d_global, d_pointer)
        ! Inputs
        real(8), intent(in) :: d_local(:,:)
        integer, intent(in) :: lenA
        integer, intent(in) :: lenB
        integer, intent(in) :: m

        ! Outputs
        real(8), intent(out) :: d_global(lenA+1, lenB+1)
        real(8), intent(out) :: d_pointer(lenA+1, lenB+1)

        ! Local variables
        real(8) :: choices(3)
        real(8) :: minimum
        integer :: min_ptr
        integer :: num_minima

        integer :: i, j, k

        ! Checks that d_local (the local difference matrix) is a numpy array
        ! An empty global difference matrix (d_local) and empty d_pointer array are created
        ! with the same dimensions as d_local
        d_global = 0
        d_pointer = 0

        ! the first row and column of d_global are set to infinity
        d_global(1,:) = inf()
        d_global(:,1) = inf()
        d_global(1,1) = 0.

        ! this loop goes through and fills in d_global and d_pointer
        do i=2, lenA+1
            do j=2, lenB+1
                ! This is the region outside the window so top left and bottom right
                if ((m > 0) .and. (abs(i-j) > m)) then
                    d_global(i,j) = inf()
                    d_pointer(i,j) = inf()

                ! this is the region to the bottom left
                else if ((m > 0) .and. ((i+j) < (m+4))) then
                    d_global(i,j) = inf()
                    d_pointer(i,j) = 0

                ! this is the line at the bottom left
                else if ((m > 0) .and. ((i+j) == (m+4))) then
                    d_global(i,j) = 0
                    d_pointer(i,j) = 0

                ! this is everything within the search window, including the region
                ! at the top right
                else
                    ! added by SThomas on 26Nov19 - to combat the multiple choices
                    ! the algorithm can make when choosing the minimum
                    choices = [ d_global(i-1,j-1) + (2.0*d_local(i,j)), & ! down and to the left
                                d_global(i-1,j) + d_local(i,j),         & ! down
                                d_global(i,j-1) + d_local(i,j)          ] ! left

                    minimum = minval(choices)
                    min_ptr = minloc(choices, dim=1)-1
                    num_minima = 0
                    do k=1, 3
                        if (choices(k) == minimum) num_minima = num_minima + 1
                    end do
                    ! array that are a minimum

                    if (num_minima == 1) then ! if just one minimum, do as previously, set the
                    ! d_global element and d_pointer element as the minimum
                        d_global(i,j) = minimum
                        d_pointer(i,j) = min_ptr

                    else ! len(inds) is always 1 <= len <= 3, so only goes through here if
                    ! there are 2 or 3 minima
                        if (i == j) then ! on the i = j line
                            if (choices(1) == minimum) then ! if 0 is an option, tell it to carry on
                            !  down the i = j line
                                d_global(i,j)  = choices(1)
                                d_pointer(i,j) = 0
                            else ! otherwise, I don't know how to pick between these two,
                            !  so currently it will just print this out as a flag and will
                            !  need to be reconsidered. By default, it picks the downward
                            !  option
                            !    if ((i + j) /= (m + 3)) then
                                !    print *, 'It is on the i = j line when (i,j) = (',i,',',j,') and can choose down or left'
                                !    print *, 'Please inform the developer'
                            !    end if
                                d_global(i,j) = minimum
                                d_pointer(i,j) = min_ptr
                            end if

                        else if (i > j) then ! this is above the i = j line.
                            if (choices(2) == minimum) then ! if 1 is an option, tell it to move
                            ! down and towards the i = j line
                                d_global(i,j) = choices(2)
                                d_pointer(i,j) = 1
                            else ! otherwise choose the option that moves it parallel to the i = j line
                                d_global(i,j) = choices(1)
                                d_pointer(i,j) = 0
                            end if

                        else ! to the right of the i = j line
                            if (choices(3) == minimum) then ! if 2 is an option, move it left towards the i = j line
                                d_global(i,j) = choices(3)
                                d_pointer(i,j) = 2
                            else ! otherwise choose the option that moves it parallel to the i = j line
                                d_global(i,j) = choices(1)
                                d_pointer(i,j) = 0
                            end if
                        end if
                    end if
                end if
            end do
        end do
        ! return the global difference matrix and the d_pointer matrix,

    end subroutine global_distance_new

end module dtw_f
