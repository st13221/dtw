################################################################################
# Copyright 2017-2023 Steven Thomas, University of York
#
# SPDX-License-Identifier: LGPL-3.0-or-later
#
# DTW is free software: you can redistribute it and/or modify it under
# the terms of the GNU Lesser General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# DTW is distributed in the hope that it will be useful, but WITHOUT
# ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
# or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General
# Public License for more details.
#
# You should have received a copy of the GNU Lesser General Public License
# along with DTW. If not, see <http://www.gnu.org/licenses/>.
################################################################################


import numpy as np
import matplotlib.pyplot as plt
from datetime import datetime
import random
from scipy import interpolate
import numpy.ma as ma
import os.path
from dtw_f import dtw_f
from math import ceil, floor


# seed_number = 123456
# random.seed(seed_number)
# np.random.seed(seed_number)

def filecheck_func(fname):
    """Select a unique filename. If ``fname`` already exists, append
    increasing integers until the filename is unique.

    The new filename is printed to screen, as well as returned
    """

    filecheck = os.path.isfile(fname)
    #if it doesn't then the fname is passed back out the function
    if filecheck == False:
        return fname
    else:
        #prints to tell the user the file already exists
        print("file {} already exists \n".format(fname))
        #starts loop to try new names
        while filecheck == True:
            #this line removes the '.npz' from the old filename, and then adds
            #'_1.npz' - it then checks the new filename
            fname = fname[:-4] + str('_1.npz')
            filecheck = os.path.isfile(fname)
        #prints out the new filename for the user
        print("so the file has been renamed to {}".format(fname))
        return fname


def frame_normaliser(frame):
    """Normalise frame to its maximum value

    This should only be used on frames that have been blurred first

    Parameters
    ----------
    frame : array_like

    Returns
    -------
    array_like
        Normalised frame with the same shape as original frame
    """

    if np.amax(frame) > 0:
        return frame / np.amax(frame)
    else:
        return frame


def local_distance(A, B):
    """Create local difference matrix from 1D arrays

    The returned matrix has shape ``(len(A) + 1, len(B) + 1)``, and
    the first row and column are filled with `numpy.inf`

    Parameters
    ----------
    A : array_like
        1D signal array
    B : array_like
        1D signal array

    Returns
    -------
    Tuple[array_like, int, int]
        Local difference matrix, and the lengths of signals A and B
    """
    #checks that the inputs are numpy arrays and the correct 1D shape
    if len(A.shape) != 1:
        raise ValueError("A is not a 1D numpy array")
    if len(B.shape) != 1:
        raise ValueError("B is not a 1D numpy array")
    #make the lengths of the signals variables
    lenA = len(A)
    lenB = len(B)
    #d = the local difference matrix with extra element of infinity for
    #the algorithm to use min correctly with
    d = np.zeros((lenA + 1, lenB + 1))
    d[0,1:] = np.inf
    d[1:,0] = np.inf
    #fills in the whole local difference matrix d
    for i in range(0,lenA):
        for j in range(0,lenB):
            d[i+1,j+1] = abs(A[i]-B[j])

    return d, lenA, lenB


def local_distance_new(A, B, alpha, fast=True):
    """Create local distance matrix from 2D arrays, comparing the last
    dimension only.

    The returned matrix has shape ``(A.shape[-1] + 1, B.shape[-1] +
    1)``, and the first row and column are filled with `numpy.inf`

    Parameters
    ----------
    A : array_like
        2D signal array
    B : array_like
        2D signal array
    alpha : str
        Choice of window function: either ``"alpha"`` for the alpha
        window function as described in Quenot 1998; any other choice
        is an array of ones
    fast : bool
        If ``True`` use the compiled Fortran implementation, otherwise
        use the pure-Python implementation

    Returns
    -------
    np.ndarray, int, int
        The distance matrix, length of signal A, length of signal B

    """

    # here we assume the strips are the same width, and they should be.
    # p is orthogonal to stripping direction
    width = A.shape[0]
    if alpha == 'cosine':
        p = np.linspace(-width/2., width/2., width)
        window = 1. + np.cos(2. * np.pi * p / float(width))
    else:
        window = np.ones(width)

    if fast: # SThomas added fast bool 14/aug/2020
        return dtw_f.local_distance_new(A, B, window)

    lenA = A.shape[-1]
    lenB = B.shape[-1]

    #d = the local difference matrix with extra element of infinity for
    #the algorithm to use min correctly with
    d = np.zeros((lenA + 1, lenB + 1))
    d[0,1:] = np.inf
    d[1:,0] = np.inf
    #fills in the whole local difference matrix d
    for i in range(0,lenA):
        for j in range(0,lenB):
            d[i+1,j+1] = np.sum(window * np.abs(A[:,i] - B[:,j]))

    return d, lenA, lenB


def global_distance_new(d, lenA, lenB, m, fast=True): # SThomas added fast bool 14/aug/2020
    """Create global distance matrix and pointer matrix

    The elements in the pointer matrix are either 0 (diagonally down
    and left), 1 (down), or 2 (left), and is used when finding the
    warp path

    Parameters
    ----------
    d : array_like
        Local difference matrix
    lenA : int
        Length of signal A
    lenB : int
        Length of signal B
    m : int
        Number of adjacent elements to search
    fast : bool
        Use compiled Fortran algorithm

    """

    if fast: # SThomas added fast bool 14/aug/2020
        if m != None: # SThomas added 6/aug/2020 - allows use of no m
            return dtw_f.global_distance_new(d, lenA, lenB, m)
        else: # SThomas added 6/aug/2020 - allows use of no m
            return dtw_f.global_distance_new(d, lenA, lenB, 0) # SThomas added 6/aug/2020 - allows use of no m
    #checks that d (the local difference matrix) is a numpy array
    if not isinstance(d, np.ndarray):
        raise ValueError("d is not a numpy array.")
    #An empty global difference matrix (D) and empty pointer array are created
    #with the same dimensions as d
    D = np.zeros_like(d)
    pointer = np.zeros_like(d)
    #the first row and column of D are set to infinity
    D[0,1:] = np.inf
    D[1:,0] = np.inf
    # D[0,0] = np.inf
    #this loop goes through and fills in D and pointer
    for i in range(1,lenA+1):
        for j in range(1,lenB+1):
            #This is the region outside the window so top left and bottom right
            if (m != None) and (abs(i-j) > m): # SThomas 6/Aug/2020 - commented out these lines
                D[i,j] = np.inf
                pointer[i,j] = np.inf
            #this is the region to the bottom left
            elif (m != None) and ((i + j) < (m + 2)): # SThomas 6/Aug/2020 - commented out these lines
                D[i,j] = np.inf
                pointer[i,j] = 0
            #this is the line at the bottom left
            elif (m != None) and ((i + j) == (m + 2)): # SThomas 6/Aug/2020 - commented out these lines
                D[i,j] = 0
                pointer[i,j] = 0
            #this is everything within the search window, including the region
            #at the top right
            else:
                a = D[i-1,j-1] + (2.0*d[i,j]) #down and to the left
                b = D[i-1,j] + d[i,j] #down
                c = D[i,j-1] + d[i,j] #left
                #added by SThomas on 26Nov19 - to combat the multiple choices
                #the algorithm can make when choosing the minimum
                choices = np.array((a, b, c)) #the three options
                inds = np.where(choices == choices.min())[0] #indices of that options
                #array that are a minimum
                if len(inds) == 1: #if just one minimum, do as previously, set the
                #D element and pointer element as the minimum
                    D[i,j] = choices[inds[0]]
                    pointer[i,j] = inds[0]
                else: #len(inds) is always 1 <= len <= 3, so only goes through here if
                #there are 2 or 3 minima
                    if i == j: #on the i = j line
                        if np.any(inds == 0): #if 0 is an option, tell it to carry on
                        # down the i = j line
                            D[i,j] = choices[0]
                            pointer[i,j] = 0
                        else: #otherwise, I don't know how to pick between these two,
                        # so currently it will just print this out as a flag and will
                        # need to be reconsidered. By default, it picks the downward
                        # option
                            # if (i + j) != (m + 3):
                            #     print('It is on the i = j line when (i,j) = ('+\
                            #             str(i)+','+str(j)+') and can choose down or left')
                            #     print('Please inform the developer')
                            D[i,j] = choices[inds[0]]
                            pointer[i,j] = inds[0]
                    elif i > j: #this is above the i = j line.
                        if np.any(inds == 1): #if 1 is an option, tell it to move
                        #down and towards the i = j line
                            D[i,j] = choices[1]
                            pointer[i,j] = 1
                        else: #otherwise choose the option that moves it parallel to
                        #the i = j line
                            D[i,j] = choices[0]
                            pointer[i,j] = 0
                    else: #to the right of the i = j line
                        if np.any(inds == 2): #if 2 is an option, move it left towards
                        #the i = j line
                            D[i,j] = choices[2]
                            pointer[i,j] = 2
                        else: #otherwise choose the option that moves it parallel to
                        #the i = j line
                            D[i,j] = choices[0]
                            pointer[i,j] = 0

    return D, pointer


def global_distance(d, lenA, lenB, m):
    """Create global distance matrix and pointer matrix

    The elements in the pointer matrix are either 0 (diagonally down
    and left), 1 (down), or 2 (left), and is used when finding the
    warp path

    Parameters
    ----------
    d : array_like
        Local difference matrix
    lenA : int
        Length of signal A
    lenB : int
        Length of signal B
    m : int
        Number of adjacent elements to search

    """
    #checks that d (the local difference matrix) is a numpy array
    if not isinstance(d, np.ndarray):
        raise ValueError("d is not a numpy array.")
    #An empty global difference matrix (D) and empty pointer array are created
    #with the same dimensions as d
    D = np.zeros_like(d)
    pointer = np.zeros_like(d)
    #the first row and column of D are set to infinity
    D[0,1:] = np.inf
    D[1:,0] = np.inf
    D[0,0] = np.inf
    #this loop goes through and fills in D and pointer
    for i in range(1,lenA+1):
        for j in range(1,lenB+1):
            #This is the region outside the window so top left and bottom right
            if abs(i-j) > m:
                D[i,j] = np.inf
                pointer[i,j] = np.inf
            #this is the region to the bottom left
            elif (i + j) < (m + 2):
                D[i,j] = np.inf
                pointer[i,j] = 0
            #this is the line at the bottom left
            elif (i + j) == (m + 2):
                D[i,j] = 0
                pointer[i,j] = 0
            #this is everything within the search window, including the region
            #at the top right
            else:
                a = D[i-1,j-1] + (2.0*d[i,j])
                b = D[i-1,j] + d[i,j]
                c = D[i,j-1] + d[i,j]
                D[i,j] = min(a, b, c)
                pointer[i,j] = np.argmin((a, b, c))
#                if pointer[i,j] == 1:
#                    if b == c:
#                        print("b = c for i = {} and j = {}".format(i,j))
#                elif pointer[i,j] == 0:
#                    if a == b:
#                        print("a = b for i = {} and j = {}".format(i,j))
#                        if a == c:
#                            print("a also = c for i = {} and j = {}".format(i,j))
#                    elif a == c:
#                        print("a = c for i = {} and j = {}".format(i,j))
    return D, pointer


def path_finder_no_m(D, pointer, lenA, lenB):
    """Create warp path

    Doesn't take into account element distance.

    ``D``, ``pointer`` are the outputs of `global_distance` or
    `global_distance_new`

    Parameters
    ----------
    D : array_like
        Global difference matrix
    pointer : array_like
        Pointer matrix (must be square)
    lenA : int
        Length of signal A
    lenB : int
        Length of signal B

    """

    i = pointer.shape[0] - 1
    j = pointer.shape[0] - 1
    path_distance = D[-1,-1] / (lenA + lenB)
    path1 = [i]
    path2 = [j]
    while (i > 1) and (j > 1):
        if pointer[i,j] == 1:
            i -= 1
        elif pointer[i,j] == 2:
            j -= 1
        else:
            i -= 1
            j -= 1
        #appends the indexes to the lists
        path1.append(i)
        path2.append(j)
    path1.reverse()
    path2.reverse()
    #easiest to have a single numpy array rather than two lists.
    path = np.column_stack((path1,path2))
    return path


def path_finder_new(D, pointer, lenA, lenB, m):
    """Create warp path, allowing for multiple minima on the ``D_end`` line.

    Chooses minima closest to ``i == j``.

    ``D``, ``pointer`` are the outputs of `global_distance` or
    `global_distance_new`

    Parameters
    ----------
    D : array_like
        Global difference matrix
    pointer : array_like
        Pointer matrix (must be square)
    lenA : int
        Length of signal A
    lenB : int
        Length of signal B
    m : int
        Number of adjacent elements to search

    """

    #gets the size of the pointer matrix (in my work it will always be square)
    pointer_len = pointer.shape[0]
    #creates the i and j indexes for all elements on the end line
    I = np.linspace(pointer_len-1, pointer_len-m-1,m+1).astype(int)
    J = I[::-1]
    #finds the minimum of all the global differences on the finish line
    D_end = np.zeros(m+1)
    for k in range(0, m+1):
        D_end[k] = D[I[k],J[k]]
    #finds the path difference and prints it to 5 sig figs
    path_distance = min(D_end) / (lenA + lenB)
    #this finds the index of the minimum in D_end, then the next two lines
    #find the i and j index that corresponds to that value in the D matrix
    #SThomas changed 26Nov19 - adding in D_end logic
    inds = np.where(D_end == D_end.min())[0] #create inds of minima on D_end
    if len(inds) == 1: #if only 1 minima, then use it same as before
        index = inds[0] #setting the index
    else: #inds will always be 1 or more elements, so only need 1 else
        inds_minus = np.abs(inds - (m//2)) #makes the i=j index 0, and the others
        #positively increasing with distance from it either side
        inds_min = np.where(inds_minus == inds_minus.min())[0] #returns the minima
        #of the altered inds. Will either be len() 1 or 2
        index = inds[inds_min[0]] #regardless of size, return the first element.
        #if only 1 long it will return what is the minima closest to i=j. If 2 long
        #it will return just one, the one where i > j
        # if len(inds_min) > 1: #checks if there were two equidistant minima and
        # #prints a flag to tell the user. It will then need looking at again.
        #     print('There were two equidistant D_end minima')
        #     print('Please inform the developer')
    i = I[index]
    j = J[index]
    #this is the minimum global difference on the finish line
    D_min = D[i,j]
    #creates a list of the different i and j indexes
    path1 = [i]
    path2 = [j]
    #whilst both the indexes are less than the pointer_len (so both are still
    #in the matrix). It starts on the finish line and works its way to the top
    #right edge in a diagonal line. Appends the element to the path lists.
    while((i < pointer_len-1 and j < pointer_len-1)):
        i += 1
        j += 1
        path1.append((i))
        path2.append((j))
    #reverses the lists so they are in the correct order, then resets to the
    #finish line.
    path1.reverse()
    path2.reverse()
    i = I[index]
    j = J[index]
    #uses m+3 because m+2 accounts for the matrix being 1 longer in each
    #direction because of the extra line for infinities added. The extra 1 is
    #then because if the algorithm gets to an element that has the start line
    #directly below and to the side of it then it will just pick the one below
    #because of how the argmin works. Instead, it should go diagonally, so it's
    #easiest to stop the algorithm here and then just tell it to go diagonally,
    #which is the next step anyway.
    while((i+j)>(m+3)):
        if pointer[i,j] == 1:
            i -= 1
        elif pointer[i,j] == 2:
            j -= 1
        else:
            i -= 1
            j -= 1
        #appends the indexes to the lists
        path1.append(i)
        path2.append(j)
    #iterates back to the first i and j element matched.
    while((i>1 and j>1)):
        i -= 1
        j -= 1
    #appends it to the lists
        path1.append(i)
        path2.append(j)
    #reverse the lists again
    path1.reverse()
    path2.reverse()
    #easiest to have a single numpy array rather than two lists.
    path = np.column_stack((path1,path2))
    check = np.absolute(path[:,0] - path[:,1])
    #if np.amax(check) == m:
    #    print("There is an instance of the path displacment equalling m :(")
    return path


def path_finder(D, pointer, lenA, lenB, m):
    """Create warp path

    ``D``, ``pointer`` are the outputs of `global_distance` or
    `global_distance_new`

    Parameters
    ----------
    D : array_like
        Global difference matrix
    pointer : array_like
        Pointer matrix (must be square)
    lenA : int
        Length of signal A
    lenB : int
        Length of signal B
    m : int
        Number of adjacent elements to search

    """
    #gets the size of the pointer matrix (in my work it will always be square)
    pointer_len = pointer.shape[0]
    #creates the i and j indexes for all elements on the end line
    I = np.linspace(pointer_len-1, pointer_len-m-1,m+1).astype(int)
    J = I[::-1]
    #finds the minimum of all the global differences on the finish line
    D_end = np.zeros(m+1)
    for k in range(0, m+1):
        D_end[k] = D[I[k],J[k]]
    #finds the path difference and prints it to 5 sig figs
    path_distance = min(D_end) / (lenA + lenB)
    #this finds the index of the minimum in D_end, then the next two lines
    #find the i and j index that corresponds to that value in the D matrix
    index = np.argmin(D_end)
    i = I[index]
    j = J[index]
    #this is the minimum global difference on the finish line
    D_min = D[i,j]
    #creates a list of the different i and j indexes
    path1 = [i]
    path2 = [j]
    #whilst both the indexes are less than the pointer_len (so both are still
    #in the matrix). It starts on the finish line and works its way to the top
    #right edge in a diagonal line. Appends the element to the path lists.
    while((i < pointer_len-1 and j < pointer_len-1)):
        i += 1
        j += 1
        path1.append((i))
        path2.append((j))
    #reverses the lists so they are in the correct order, then resets to the
    #finish line.
    path1.reverse()
    path2.reverse()
    i = I[index]
    j = J[index]
    #uses m+3 because m+2 accounts for the matrix being 1 longer in each
    #direction because of the extra line for infinities added. The extra 1 is
    #then because if the algorithm gets to an element that has the start line
    #directly below and to the side of it then it will just pick the one below
    #because of how the argmin works. Instead, it should go diagonally, so it's
    #easiest to stop the algorithm here and then just tell it to go diagonally,
    #which is the next step anyway.
    while((i+j)>(m+3)):
        if pointer[i,j] == 1:
            i -= 1
        elif pointer[i,j] == 2:
            j -= 1
        else:
            i -= 1
            j -= 1
        #appends the indexes to the lists
        path1.append(i)
        path2.append(j)
    #iterates back to the first i and j element matched.
    while((i>1 and j>1)):
        i -= 1
        j -= 1
    #appends it to the lists
        path1.append(i)
        path2.append(j)
    #reverse the lists again
    path1.reverse()
    path2.reverse()
    #easiest to have a single numpy array rather than two lists.
    path = np.column_stack((path1,path2))
    check = np.absolute(path[:,0] - path[:,1])
    #if np.amax(check) == m:
    #    print("There is an instance of the path displacment equalling m :(")
    return path


def path_changer(path):
    """Takes the path originally found with DTW and decides 4
    different ways of determining the matching of the signals.  path_1
    and path_3 find any duplicate elements from signal A. path_1 keeps
    just the first element and path_3 keeps just the last element.
    path_2 and path_4 do the same thing with signal B, path_2 keeping
    the first duplicate element, and path_4 keeping the last duplicate
    element.

    """

    #nums and counts creates 2 arrays for both list of elements in the path.
    #nums reduces the elements down so there are no duplicate numbers, and
    #counts keeps a record of how many instances of the element there were.
    I_nums, I_counts = np.unique(path[:,0], return_counts=True)
    #mult returns the indexes from counts where the number is greater than 1.
    #This index also corresponds to the element number in nums.
    mult_I = np.where(I_counts > 1)[0]
    #path_1
    path_new = path
    #if len(mult_I) == 0 then there were no instances of repeated elements in
    #the first part of the the path.
    if len(mult_I) > 0:
        #this loop adapts to the number of times it has to go through and delete
        #elements.
        for i in range(0, len(mult_I)):
            #elem_I is the element number to be deleted
            elem_I = I_nums[mult_I[i]]
            #finds and returns indexes in path where the element number is, but
            #not the first instance, as that one is to be kept.
            to_del = np.where(path_new[:,0] == elem_I)[0][1:]
            #deletes the duplicate rows
            path_new = np.delete(path_new, to_del, 0)

    return path_new


def velocimetry(x, path):
    """Create "velocity" field from altered path, mapping signal A to signal B

    Parameters
    ----------
    x : array_like
        Signal A
    path : array_like
        Warp path

    Returns
    -------
    position, displacement
    """

    #takes the difference in the elements and converts it to
    #absolute displacement using the x coordinates
    displacement = path[:,1] - path[:,0]
    displacement = displacement * np.mean(np.diff(x))
    #elements is the index's to use to show position
    # elements = path[:,0] - 1
    # #position is the position of signal A in this case
    # position = np.zeros_like(elements).astype(float)
    # #turn element number into position with this loop
    # for i in range(0,len(elements)):
    #     position[i] = x[elements[i]]
    position = x[path[:,0] - 1]
    return position, displacement


def blob_maker(A, x, y, x0, y0, sigma_x, sigma_y, angle=0., units='deg'):
    """Create two elliptic Gaussians

    Used for testing the 2D DTW

    Parameters
    ----------
    A : float
        Amplitude of Gaussians
    x : array_like
        1D or 2D x coordinate array
    y : array_like
        1D or 2D y coordinate array
    x0 : float
        Centre of Gaussians in x
    y0 : float
        Centre of Gaussians in y
    sigma_x : float
        Variance of Gaussians in x
    sigma_y : float
        Variance of Gaussians in y
    angle : float
        Angle of ellipse
    units : str
        Either ``"deg"`` to give ``angle`` in degrees; or ``"rad"`` to
        give ``angle`` in radians

    """


    if x.ndim == y.ndim == 1:
        x, y = np.meshgrid(x, y)
    if units == 'deg':
        angle = np.deg2rad(angle)
    elif units != 'rad':
        print('kwarg units input not understood.')
        print('Should be a string of deg or rad. angle reset to 0.')
        angle = 0.
    a = (np.cos(angle) / (np.sqrt(2) * sigma_x))**2 + \
            (np.sin(angle) / (np.sqrt(2) * sigma_y))**2
    b = (-np.sin(2. * angle) / (2. * sigma_x**2)) + \
            (np.sin(2. * angle) / (2. * sigma_y**2))
    c = (np.sin(angle) / (np.sqrt(2) * sigma_x))**2 + \
            (np.cos(angle) / (np.sqrt(2) * sigma_y))**2
    return A * np.exp(-(a * (x - x0)**2 + b * (x - x0) * (y - y0) + c * (y - y0)**2))


def whole_width(position, displacement, width):
    """Extrapolate displacements to whole width of signal

    Parameters
    ----------
    position : array_like
    displacement : array_like
    width : int
        Width of whole signal
    """

    #check if the position and displacement are initially the same length
    if not len(position) == len(displacement):
        raise ValueError("The position and displacement aren't the same length"\
        " to begin with.")
    #find the first x position, then minus 1 because of python indexing
    index1 = position[0].astype(int) - 1
    #find the final x position. Don't minus 1.
    index2 = position[-1].astype(int)
    #create empty array the width of the image, then put the displacement values
    #into the correct place
    whole_displacement = np.zeros(width)
    whole_displacement[index1:index2] = displacement
    #if the first position was 1, then it won't go through the next statement,
    #but if it wasn't then it extrapolates the displacement value at the left
    #most position outwards left
    if index1 != 0:
        whole_displacement[:index1] = displacement[0]
    #similarly, this checks if the original position went as far the 128 mark
    #(128 for x, 64 for y). If it didn't then it extrapolates the right most
    #displacement.
    if index2 != width:
        whole_displacement[index2:] = displacement[-1]
    return whole_displacement


def the_x_distorter(xx, x_displacement):
    """Check for shape differences between original coordinates and
    displacements, then computes and returns new coordinates"""

    xx2 = xx + x_displacement
    return xx2


def the_y_distorter(yy, y_displacement):
    """Check for shape differences between original coordinates and
    displacements, then computes and returns new coordinates"""

    yy2 = yy + y_displacement
    return yy2


def duplicate_x_finder_1D_interp(xx, z, x):
    """Interpolate altered image back into original x grid coordinates

    FIXME: Rename

    Parameters
    ----------
    xx : array_like
        2D altered x coordinate array
    z : array_like
        2D altered image
    x : array_like
        1D original x coordinates
    """

    len_y = z.shape[0]
    #create empty array for new z
    z_new = np.zeros_like(z)
    #for loop to go through each row
    for i in range(0,len_y):
        #interpolate uses linear and fills values outside the domain with zeros
        f = interpolate.interp1d(xx[i,:], z[i,:], kind='linear', \
        bounds_error=False, fill_value=(z[i,0],z[i,-1]))
        #puts it into new z_new
        z_new[i,:] = f(x)
    return z_new


def duplicate_y_finder_1D_interp(yy, z, y):
    """Interpolate altered image back into original y grid coordinates

    FIXME: Rename

    Parameters
    ----------
    y : array_like
        2D altered y coordinate array
    z : array_like
        2D altered image
    y : array_like
        1D original y coordinates
    """

    len_x = z.shape[1]
    #create empty array for new z
    z_new = np.zeros_like(z)
    #for loop to go through each column
    for i in range(0,len_x):
        #interpolate uses linear and fills values outside the domain with zeros
        f = interpolate.interp1d(yy[:,i], z[:,i], kind='linear', \
        bounds_error=False, fill_value=(z[0,i],z[-1,i]))
        #puts into z_new
        z_new[:,i] = f(y)
    return z_new


def normaliser(z1):
    """Normalise array to maximum value if positive"""
    if np.amax(z1) > 0:
        z1 = z1 / np.amax(z1)
    return z1


def boxcar(displacement, m, l=3.0, fast=True): # SThomas added fast bool 14/aug/2020
    """Apply boxcar smoothing to ``displacement`` in order to make
    sensible looking velocities

    Parameters
    ----------
    displacement : array_like
        1D array of displacements
    m : int
        Number of adjacent elements to consider
    l : float
        If any consecutive displacement in a strip is greater than the
        window parameter divided by ``l`` (``displacement > (m / l)``)
        then smoothing is continuously applied. If ``l is None``, then
        the boxcar is not used at all.

        Found from performing a kspec on the data
    fast : bool
        If ``True`` use compiled Fortran implementation; else use the
        pure Python implementation

    Returns
    -------
    array_like
        1D array of new displacements, same length as input ``displacement``

    """

    if fast: # SThomas added fast bool 14/aug/2020
        return dtw_f.boxcar(displacement, m, l)

    #displacement will be the displacement array that is being smoothed, window
    #is the size of the boxcar that I want to use
    window = 15
    whole_len = len(displacement)
    #this is the total x or y direction
    if window % 2 == 1:
        window = window - 1
    #this makes the window an odd number all the time
    half = window // 2
    #while loop means that any time the displacement between two adjacent pixels
    #varies too much that the boxcar is run again.
    change_in_displacement = np.absolute(np.diff(displacement))
    new_displacement = np.array(displacement, copy = True)
    while np.any(change_in_displacement > (m / l)):
        #empty array to put the smoothed displacement into
        new_displacement = np.zeros_like(displacement)
        #the central half of the displacement is smoothed first
        for i in range(half, (whole_len - half)):
            new_displacement[i] = np.mean(displacement[(i-half):(i+half+1)])
        #the ends are treated slightly differently
        for i in range(0,half):
            new_displacement[i] = np.mean(displacement[:((i*2) + 1)])
            new_displacement[i+whole_len-half] = np.mean(displacement[ \
            (whole_len - (2*half) + 1 + (2*i)):])
        #change_in_displacement is the different between displacement of
        #adjacent pixels
        change_in_displacement = np.absolute(np.diff(new_displacement))
        displacement = np.array(new_displacement, copy=True)
    return new_displacement #TODO l needs sorting!


def position_interper_x(xx, oxx, oyy, x):
    """Move frame according to displacement fields

    Parameters
    ----------
    xx : array_like
        2D x coordinates
    oxx : array_like
    oyy : array_like
    x : array_lile
        1D new x coordinates

    Returns
    -------
    array_like, array_like
    """

    len_y = xx.shape[0]
    #create empty array for new z
    oxx_new = np.zeros_like(oxx)
    oyy_new = np.zeros_like(oyy)
    #for loop to go through each row
    for i in range(0,len_y):
        # if (np.diff(xx[i,:]) < 0.).all() == False:
        #     print(xx[i,:], np.diff(xx[i,:]))
        #interpolate uses linear and fills values outside the domain with nans
        f = interpolate.interp1d(xx[i,:], oxx[i,:], kind='linear', \
        bounds_error = False, fill_value=np.nan)
        #puts it into new z_new
        oxx_new[i,:] = f(x)
        # oxx_new[i,:] = np.interp(x, xx[i,:], oxx[i,:], left = np.nan, right = np.nan)
        g = interpolate.interp1d(xx[i,:], oyy[i,:], kind='linear', \
        bounds_error = False, fill_value=np.nan)
        oyy_new[i,:] = g(x)
    return oxx_new, oyy_new


def position_interper_y(yy, oxx, oyy, y):
    """Move frame according to displacement fields

    Parameters
    ----------
    yy : array_like
        2D y coordinates
    oxx : array_like
    oyy : array_like
    y : array_lile
        1D new y coordinates

    Returns
    -------
    array_like, array_like
    """
    len_x = yy.shape[1]
    #create empty array for new z
    oxx_new = np.zeros_like(oxx)
    oyy_new = np.zeros_like(oyy)
    #for loop to go through each column
    for i in range(0,len_x):
        #interpolate uses linear and fills values outside the domain with zeros
        f = interpolate.interp1d(yy[:,i], oxx[:,i], kind='linear', \
        bounds_error = False, fill_value=np.nan)
        #puts into z_new
        oxx_new[:,i] = f(y)
        g = interpolate.interp1d(yy[:,i], oyy[:,i], kind='linear', \
        bounds_error = False, fill_value=np.nan)
        oyy_new[:,i] = g(y)
    return oxx_new, oyy_new


def strip_maker_x(frame, strip_width):
    """Average ``frame`` across strips of width ``strip_width`` in x

    Parameters
    ----------
    frame : array_like
        2D array
    strip_width : int
        Width of strips in pixels

    Returns
    -------
    strips : numpy.ndarray
        The length of axis 0 is equal to the number of strips, while
        the length of axis 1 is the same as axis 1 of the input
        ``frame``
    strip_centres : numpy.ndarray
        1D of pixel number at centre of each strip (NOTE: pixel number
        NOT Python index number)

    """

    #set the sizes of the frame equal to variables
    len_y = frame.shape[0]
    len_x = frame.shape[1]
    #if function to determine the number of strips needed, depending on if
    #strips of equal length will fit along the length or not
    if (len_y * 2.) % strip_width == 0:
        number_strips = int(((len_y * 2.) // strip_width) - 1.)
    else:
        number_strips = int((len_y * 2.) // strip_width)
    #empty arrays for the strips (the mean of the intensities) and for the
    #centre pixel position of the strips
    strips = np.zeros([number_strips,len_x])
    strip_centres = np.zeros([number_strips])
    #for loop calculates the strip means and strip centres for all but the last
    #strips
    for i in range(0,number_strips - 1):
        strips[i,:] = np.mean(frame[int(i * strip_width / 2.):int(strip_width * \
        ((i / 2.) + 1)),:], axis = 0)
        strip_centres[i] = ((strip_width * (i + 1)) - 1) / 2.
    #last strip is handled differently but works for both examples of the if
    #function
    strips[-1,:] = np.mean(frame[int((number_strips - 1) * strip_width / \
    2.):,:], axis = 0)
    strip_centres[-1] = (((number_strips - 1) * strip_width / 2.) + len_y + 1) \
    / 2.
    return strips, strip_centres


def strip_maker_x_new(frame, strip_width):
    """Average ``frame`` across strips of width ``strip_width`` in y

    Parameters
    ----------
    frame : array_like
        2D array
    strip_width : int
        Width of strips in pixels

    Returns
    -------
    strips : numpy.ndarray
        The length of axis 0 is equal to the number of strips, while
        the length of axis 1 is the same as axis 1 of the input
        ``frame``
    strip_centres : numpy.ndarray
        1D of pixel number at centre of each strip (NOTE: pixel number
        NOT Python index number)

    """

    #set the sizes of the frame equal to variables
    len_y = frame.shape[0]
    len_x = frame.shape[1]
    #if function to determine the number of strips needed, depending on if
    #strips of equal length will fit along the length or not
    if (len_y * 2.) % strip_width == 0:
        number_strips = int(((len_y * 2.) // strip_width) - 1.)
    else:
        number_strips = int((len_y * 2.) // strip_width)
    #empty arrays for the strips (the mean of the intensities) and for the
    #centre pixel position of the strips
    # strips = np.zeros([number_strips,strip_width,len_x])
    strips = [None] * number_strips
    strip_centres = np.zeros([number_strips])
    #for loop calculates the strip means and strip centres for all but the last
    #strips
    for i in range(0,number_strips - 1):
        strips[i] = frame[int(i * strip_width / 2.):int(strip_width * \
        ((i / 2.) + 1)),:]
        strip_centres[i] = ((strip_width * (i + 1)) - 1) / 2.
    #last strip is handled differently but works for both examples of the if
    #function
    strips[-1] = frame[int((number_strips - 1) * strip_width / 2.):,:]
    strip_centres[-1] = (((number_strips - 1) * strip_width / 2.) + len_y + 1) / 2.
    return strips, strip_centres


def strip_maker_y(frame, strip_width):
    """Average ``frame`` across strips of width ``strip_width`` in x

    Parameters
    ----------
    frame : array_like
        2D array
    strip_width : int
        Width of strips in pixels

    Returns
    -------
    strips : numpy.ndarray
        The length of axis 0 is the same as axis 0 of the input
        ``frame``, while the length of axis 1 is equal to the number
        of strips
    strip_centres : numpy.ndarray
        1D of pixel number at centre of each strip (NOTE: pixel number
        NOT Python index number)

    """

    #set the sizes of the frame equal to variables
    len_y = frame.shape[0]
    len_x = frame.shape[1]
    #if function to determine the number of strips needed, depending on if
    #strips of equal length will fit along the length or not
    if (len_x * 2.) % strip_width == 0:
        number_strips = int(((len_x * 2.) // strip_width) - 1.)
    else:
        number_strips = int((len_x * 2.) // strip_width)
    #empty arrays for the strips (the mean of the intensities) and for the
    #centre pixel position of the strips
    strips = np.zeros([len_y,number_strips])
    strip_centres = np.zeros([number_strips])
    #for loop calculates the strip means and strip centres for all but the last
    #strips
    for i in range(0, number_strips - 1):
        strips[:,i] = np.mean(frame[:,int(i * strip_width / 2.):int(strip_width \
        * ((i / 2.) + 1))], axis = 1)
        strip_centres[i] = ((strip_width * (i + 1)) - 1) / 2.
    #last strip is handled differently but works for both examples of the if
    #function
    strips[:,-1] = np.mean(frame[:,int((number_strips - 1) * strip_width / \
    2.):], axis = 1)
    strip_centres[-1] = (((number_strips - 1) * strip_width / 2.) + len_x + 1) \
    / 2.
    return strips, strip_centres


def strip_maker_y_new(frame, strip_width):
    """Average ``frame`` across strips of width ``strip_width`` in x

    Parameters
    ----------
    frame : array_like
        2D array
    strip_width : int
        Width of strips in pixels

    Returns
    -------
    strips : numpy.ndarray
        The length of axis 0 is the same as axis 0 of the input
        ``frame``, while the length of axis 1 is equal to the number
        of strips
    strip_centres : numpy.ndarray
        1D of pixel number at centre of each strip (NOTE: pixel number
        NOT Python index number)

    """
    #set the sizes of the frame equal to variables
    len_y = frame.shape[0]
    len_x = frame.shape[1]
    #if function to determine the number of strips needed, depending on if
    #strips of equal length will fit along the length or not
    if (len_x * 2.) % strip_width == 0:
        number_strips = int(((len_x * 2.) // strip_width) - 1.)
    else:
        number_strips = int((len_x * 2.) // strip_width)
    #empty arrays for the strips (the mean of the intensities) and for the
    #centre pixel position of the strips
    # strips = np.zeros([strip_width,len_y,number_strips])
    strips = [None] * number_strips
    strip_centres = np.zeros([number_strips])
    #for loop calculates the strip means and strip centres for all but the last
    #strips
    for i in range(0, number_strips - 1):
        strips[i] = np.swapaxes(frame[:,int(i * strip_width / 2.):int(strip_width \
        * ((i / 2.) + 1))], 0, 1)
        strip_centres[i] = ((strip_width * (i + 1)) - 1) / 2.
    #last strip is handled differently but works for both examples of the if
    #function
    strips[-1] = np.swapaxes(frame[:,int((number_strips - 1) * strip_width / \
    2.):], 0, 1)
    strip_centres[-1] = (((number_strips - 1) * strip_width / 2.) + len_x + 1) \
    / 2.
    return strips, strip_centres


def displacement_strip_maker_x(x, z1, z2, strip_width, frame_bool,
                            strip_bool, m, global_distance_new_bool,
                            path_finder_new_bool, fast=True, l=3.): # SThomas added fast bool 14/aug/2020, # SThomas added l=3. kwarg 3/Feb/2021
    """Makes displacements in the x-direction

    Parameters
    ----------
    x : array_like
        Coordinate array
    z1 : array_like
        Frame 1
    z2 : array_like
        Frame 2
    strip_width : int
        Width in pixels of strips to average over
    frame_bool : bool
    strip_bool : bool
    m : int
        Number of adjacent pixels to look at
    global_distance_new_bool : bool
    path_finder_new_bool : bool
    alpha : str
    fast : bool
        Use compiled Fortran implementation
    l : float

    Returns
    -------
    Displacement strips, the same shape as the length of x multiplied
    by the number of strips, and strip centres, 1D the length of the
    number of strips

    """
    # normalise frames if required
    if frame_bool == True:
        z1 = frame_normaliser(z1)
        z2 = frame_normaliser(z2)
    # call strip_maker to set up the strips in the x-direction and the pixel
    # positoin of their centres.
    strips_1, strip_centres_1 = strip_maker_x(z1, strip_width)
    strips_2, strip_centres_2 = strip_maker_x(z2, strip_width)
    # create empty displacement array
    displacement_strips = np.zeros_like(strips_1)
    # iterate over the strips, reduce them to 1D signals, and find the
    # displacement between them
    for i in range(0,len(strip_centres_1)):
        # normalise strips if required
        if strip_bool == True:
            strips_1[i,:] = normaliser(strips_1[i,:])
            strips_2[i,:] = normaliser(strips_2[i,:])
        # runs local distance function
        local_dist, len_z1, len_z2  = local_distance(strips_1[i,:], \
        strips_2[i,:])
        # runs global distance function
        if global_distance_new_bool == True:
            global_dist, pointer = global_distance_new(local_dist, len_z1,
                                                    len_z2, m, fast=fast) # SThomas added fast bool 14/aug/2020
        else:
            global_dist, pointer = global_distance(local_dist, len_z1, len_z2, m)
        # finds the warp path between strips
        if path_finder_new_bool == True:
            path = path_finder_new(global_dist, pointer, len_z1, len_z2, m)
        else:
            path = path_finder(global_dist, pointer, len_z1, len_z2, m)
        # ammends the warp path to be 1:1
        cut_path = path_changer(path)
        # calls velocimetry function
        position, displacement = velocimetry(x, cut_path)
        # extends the displacements to the whole width of the strip
        extended_displacement = whole_width(position, displacement, len(x))
        # calls boxcar and smooths the displacement
        # if m == None: # SThomas added 6/Aug/20
        #     m = ceil(strip_width / 3.) # SThomas added 6/Aug/20 #TODO should remove hardcoded 3.
        # displacement_strips[i,:] = boxcar(extended_displacement, m, fast=fast)# SThomas added fast bool 14/aug/2020
        # SThomas ammended 6/Oct/2020 - two bugs fixed - first bug, in python 2.7,
        # ceil() returned a float instead of an integer, so crashed in path_finder(_new)
        # function. Second bug, m was being changed from None to VALUE inside the loop,
        # so subsequent loops were not using m = None as intended.
        if l != None: # SThomas 3/Feb/2021, added if statements for l, and added kwarg for l to call for boxcar
            if m != None: # SThomas 6/Oct/2020
                displacement_strips[i,:] = boxcar(extended_displacement, m,
                                                            l=l, fast=fast) # SThomas 6/Oct/2020
            else: # SThomas 6/Oct/2020
                displacement_strips[i,:] = boxcar(extended_displacement,
                                int(ceil(strip_width / 3.)), l=l, fast=fast) # SThomas 6/Oct/2020
        else: # SThomas 3/Feb/2021
            displacement_strips[i,:] = extended_displacement # SThomas 3/Feb/2021
    return displacement_strips, strip_centres_10


def displacement_strip_maker_x_new(x, z1, z2, strip_width, frame_bool, \
                                strip_bool, m, global_distance_new_bool,
                                path_finder_new_bool, alpha, fast=True, l=3.): # SThomas added fast bool 14/aug/2020 # SThomas added l=3. kwarg 3/Feb/2021
    """Makes displacements in the x-direction

    Parameters
    ----------
    x : array_like
        Coordinate array
    z1 : array_like
        Frame 1
    z2 : array_like
        Frame 2
    strip_width : int
        Width in pixels of strips to average over
    frame_bool : bool
    strip_bool : bool
    m : int
        Number of adjacent pixels to look at
    global_distance_new_bool : bool
    path_finder_new_bool : bool
    alpha : str
    fast : bool
        Use compiled Fortran implementation
    l : float

    Returns
    -------
    Displacement strips, the same shape as the length of x multiplied
    by the number of strips, and strip centres, 1D the length of the
    number of strips

    """

    # normalise frames if required
    if frame_bool == True:
        z1 = frame_normaliser(z1)
        z2 = frame_normaliser(z2)
    # call strip_maker to set up the strips in the x-direction and the pixel
    # positoin of their centres.
    strips_1, strip_centres_1 = strip_maker_x_new(z1, strip_width)
    strips_2, strip_centres_2 = strip_maker_x_new(z2, strip_width)
    # create empty displacement array
    displacement_strips = np.zeros((len(strip_centres_1), z1.shape[1]))
    # iterate over the strips, reduce them to 1D signals, and find the
    # displacement between them
    for i in range(0,len(strip_centres_1)):
        # normalise strips if required
        if strip_bool == True:
            strips_1[i] = normaliser(strips_1[i])
            strips_2[i] = normaliser(strips_2[i])
        # runs local distance function
        local_dist, len_z1, len_z2  = local_distance_new(strips_1[i],
                                        strips_2[i], alpha, fast=fast) # SThomas added fast bool 14/aug/2020
        # runs global distance function
        if global_distance_new_bool == True:
            global_dist, pointer = global_distance_new(local_dist, len_z1,
                                                    len_z2, m, fast=fast) # SThomas added fast bool 14/aug/2020
            # print(np.isinf(global_dist).all())
            # print(global_dist[1,1], np.isinf(global_dist).all())
        else:
            global_dist, pointer = global_distance(local_dist, len_z1, len_z2,
                                                                            m)
        # finds the warp path between strips
        if path_finder_new_bool == True:
            if m != None: # SThomas added 6/Aug/2020 - takes m = None into account
                path = path_finder_new(global_dist, pointer, len_z1, len_z2, m)
            else:# SThomas added 6/Aug/2020
                path = path_finder_no_m(global_dist, pointer, len_z1, len_z2) # SThomas added 6/Aug/2020
        else:
            path = path_finder(global_dist, pointer, len_z1, len_z2, m)
        # ammends the warp path to be 1:1
        cut_path = path_changer(path)
        # calls velocimetry function
        position, displacement = velocimetry(x, cut_path)
        # extends the displacements to the whole width of the strip
        extended_displacement = whole_width(position, displacement, len(x))
        # calls boxcar and smooths the displacement
        # if m == None: # SThomas added 6/Aug/20
        #     m = ceil(strip_width / 3.) # SThomas added 6/Aug/20 #TODO should remove hardcoded 3.
        # displacement_strips[i,:] = boxcar(extended_displacement, m, fast=fast)# SThomas added fast bool 14/aug/2020
        # SThomas ammended 6/Oct/2020 - two bugs fixed - first bug, in python 2.7,
        # ceil() returned a float instead of an integer, so crashed in path_finder(_new)
        # function. Second bug, m was being changed from None to VALUE inside the loop,
        # so subsequent loops were not using m = None as intended.
        if l != None: # SThomas 3/Feb/2021, added if statements for l, and added kwarg for l to call for boxcar
            if m != None: # SThomas 6/Oct/2020
                displacement_strips[i,:] = boxcar(extended_displacement, m,
                                                            l=l, fast=fast) # SThomas 6/Oct/2020
            else: # SThomas 6/Oct/2020
                displacement_strips[i,:] = boxcar(extended_displacement,
                                int(ceil(strip_width / 3.)), l=l, fast=fast) # SThomas 6/Oct/2020
        else: # SThomas 3/Feb/2021
            displacement_strips[i,:] = extended_displacement # SThomas 3/Feb/2021
    return displacement_strips, strip_centres_1


def displacement_strip_maker_y(y, z1, z2, strip_width, frame_bool, \
                            strip_bool, m, global_distance_new_bool,
                            path_finder_new_bool, fast=True, l=3.): # SThomas added fast bool 14/aug/2020 # SThomas added l=3. kwarg 3/Feb/2021
    """Makes displacements in the y-direction

    Parameters
    ----------
    y : array_like
        Coordinate array
    z1 : array_like
        Frame 1
    z2 : array_like
        Frame 2
    strip_width : int
        Width in pixels of strips to average over
    frame_bool : bool
    strip_bool : bool
    m : int
        Number of adjacent pixels to look at
    global_distance_new_bool : bool
    path_finder_new_bool : bool
    alpha : str
    fast : bool
        Use compiled Fortran implementation
    l : float

    Returns
    -------
    Displacement strips, the same shape as the length of x multiplied
    by the number of strips, and strip centres, 1D the length of the
    number of strips

    """
    # normalise frames if required
    if frame_bool == True:
        z1 = frame_normaliser(z1)
        z2 = frame_normaliser(z2)
    # call strip_maker to set up the strips in the y-direction and the pixel
    # positoin of their centres.
    strips_1, strip_centres_1 = strip_maker_y(z1, strip_width)
    strips_2, strip_centres_2 = strip_maker_y(z2, strip_width)
    # create empty displacement array
    displacement_strips = np.zeros_like(strips_1)
    # iterate over the strips, reduce them to 1D signals, and find the
    # displacement between them
    for i in range(0,len(strip_centres_1)):
        # normalise strips if required
        if strip_bool == True:
            strips_1[:,i] = normaliser(strips_1[:,i])
            strips_2[:,i] = normaliser(strips_2[:,i])
        # runs local distance function
        local_dist, len_z1, len_z2 = local_distance(strips_1[:,i], \
        strips_2[:,i])
        # runs global distance function
        if global_distance_new_bool == True:
            global_dist, pointer = global_distance_new(local_dist, len_z1,
                                                        len_z2, m, fast=fast) # SThomas added fast bool 14/aug/2020
        else:
            global_dist, pointer = global_distance(local_dist, len_z1, len_z2,
                                                                            m)
        # ammends the warp path to be 1:1
        if path_finder_new_bool == True:
            path = path_finder_new(global_dist, pointer, len_z1, len_z2, m)
        else:
            path = path_finder(global_dist, pointer, len_z1, len_z2, m)
        cut_path = path_changer(path)
        # calls velocimetry function
        position, displacement = velocimetry(y, cut_path)
        # extends the displacements to the whole width of the strip
        extended_displacement = whole_width(position, displacement, len(y))
        # calls boxcar and smooths the displacement
        # if m == None: # SThomas added 6/Aug/20
        #     m = ceil(strip_width / 3.) # SThomas added 6/Aug/20 #TODO should remove hardcoded 3.
        # displacement_strips[:,i] = boxcar(extended_displacement, m, fast=fast) # SThomas added fast bool 14/aug/2020
        # SThomas ammended 6/Oct/2020 - two bugs fixed - first bug, in python 2.7,
        # ceil() returned a float instead of an integer, so crashed in path_finder(_new)
        # function. Second bug, m was being changed from None to VALUE inside the loop,
        # so subsequent loops were not using m = None as intended.
        if l != None: # SThomas 3/Feb/2021, added if statements for l, and added kwarg for l to call for boxcar
            if m != None: # SThomas 6/Oct/2020
                displacement_strips[:,i] = boxcar(extended_displacement, m,
                                                            l=l, fast=fast) # SThomas 6/Oct/2020
            else: # SThomas 6/Oct/2020
                displacement_strips[:,i] = boxcar(extended_displacement,
                                int(ceil(strip_width / 3.)), l=l, fast=fast) # SThomas 6/Oct/2020
        else: # SThomas 3/Feb/2021
            displacement_strips[:,i] = extended_displacement # SThomas 3/Feb/2021
    return displacement_strips, strip_centres_1


def displacement_strip_maker_y_new(y, z1, z2, strip_width, frame_bool, \
                                strip_bool, m, global_distance_new_bool,
                                path_finder_new_bool, alpha, fast=True, l=3): # SThomas added fast bool 14/aug/2020 # SThomas added l=3. kwarg 3/Feb/2021
    """Makes displacements in the y-direction

    Parameters
    ----------
    y : array_like
        Coordinate array
    z1 : array_like
        Frame 1
    z2 : array_like
        Frame 2
    strip_width : int
        Width in pixels of strips to average over
    frame_bool : bool
    strip_bool : bool
    m : int
        Number of adjacent pixels to look at
    global_distance_new_bool : bool
    path_finder_new_bool : bool
    alpha : str
    fast : bool
        Use compiled Fortran implementation
    l : float

    Returns
    -------
    Displacement strips, the same shape as the length of x multiplied
    by the number of strips, and strip centres, 1D the length of the
    number of strips

    """
    # normalise frames if required
    if frame_bool == True:
        z1 = frame_normaliser(z1)
        z2 = frame_normaliser(z2)
    # call strip_maker to set up the strips in the y-direction and the pixel
    # positoin of their centres.
    strips_1, strip_centres_1 = strip_maker_y_new(z1, strip_width)
    strips_2, strip_centres_2 = strip_maker_y_new(z2, strip_width)
    # create empty displacement array
    displacement_strips = np.zeros((z1.shape[0], len(strip_centres_1)))
    # iterate over the strips, reduce them to 1D signals, and find the
    # displacement between them
    for i in range(0,len(strip_centres_1)):
        # normalise strips if required
        if strip_bool == True:
            strips_1[i] = normaliser(strips_1[i])
            strips_2[i] = normaliser(strips_2[i])
        # runs local distance function
        local_dist, len_z1, len_z2 = local_distance_new(strips_1[i],
                                        strips_2[i], alpha, fast=fast) # SThomas added fast bool 14/aug/2020
        # runs global distance function
        if global_distance_new_bool == True:
            global_dist, pointer = global_distance_new(local_dist, len_z1,
                                                    len_z2, m, fast=fast) # SThomas added fast bool 14/aug/2020
        else:
            global_dist, pointer = global_distance(local_dist, len_z1, len_z2,
                                                                            m)
        # ammends the warp path to be 1:1
        if path_finder_new_bool == True:
            if m != None: # SThomas added 6/Aug/2020 - takes m = None into account
                path = path_finder_new(global_dist, pointer, len_z1, len_z2, m)
            else:# SThomas added 6/Aug/2020
                path = path_finder_no_m(global_dist, pointer, len_z1, len_z2) # SThomas added 6/Aug/2020
        else:
            path = path_finder(global_dist, pointer, len_z1, len_z2, m)
        cut_path = path_changer(path)
        # calls velocimetry function
        position, displacement = velocimetry(y, cut_path)
        # extends the displacements to the whole width of the strip
        extended_displacement = whole_width(position, displacement, len(y))
        # calls boxcar and smooths the displacement
        # if m == None: # SThomas added 6/Aug/20
        #     m = ceil(strip_width / 3.) # SThomas added 6/Aug/20 #TODO should remove hardcoded 3.
        # displacement_strips[:,i] = boxcar(extended_displacement, m, fast=fast) # SThomas added fast bool 14/aug/2020
        # SThomas ammended 6/Oct/2020 - two bugs fixed - first bug, in python 2.7,
        # ceil() returned a float instead of an integer, so crashed in path_finder(_new)
        # function. Second bug, m was being changed from None to VALUE inside the loop,
        # so subsequent loops were not using m = None as intended.
        if l != None: # SThomas 3/Feb/2021, added if statements for l, and added kwarg for l to call for boxcar
            if m != None: # SThomas 6/Oct/2020
                displacement_strips[:,i] = boxcar(extended_displacement, m,
                                                            l=l, fast=fast) # SThomas 6/Oct/2020
            else: # SThomas 6/Oct/2020
                displacement_strips[:,i] = boxcar(extended_displacement,
                            int(ceil(strip_width / 3.)), l=l, fast=fast) # SThomas 6Oct2020
        else: # SThomas 3/Feb/2021
            displacement_strips[:,i] = extended_displacement # SThomas 3/Feb/2021
    return displacement_strips, strip_centres_1


def displacement_maker_x_2D(displacement_strips, strip_centres, x, y):
    """After the x-displacements have been made in 1D strips, this
    function is called to make it a full 2D displacement using 1D
    interpolation

    Returns
    -------
    2D numpy array the same shape as the frames

    """
    # creates empty displacement array the same shape as the frames
    x_displacement = np.zeros([len(y), len(x)])
    ### SThomas 8Jul2022 - error when the first strip is the same width
    # as one of the dimensions, so the interpolation fails. This if
    # statement has been brought in to fix that. ###
    # if len(displacement_strips) > 1:    # proceed as normal, sthomas 8Jul22
    ### SThomas 10Mar2023 - len isn't a good way to check this, instead
    # changing to look at the specific dimension size
    if displacement_strips.shape[0] > 1:    # proceed as normal, sthomas 10Mar23
        # iterates over the number of x pixels
        for i in range(0,len(x)):
            # interpolates for all y positions at pixel x_i
            f = interpolate.interp1d(strip_centres, displacement_strips[:,i], \
            kind = 'linear', bounds_error = False, fill_value = \
            (displacement_strips[0,i], displacement_strips[-1,i]))
            x_displacement[:,i] = f(y)
    # new else statement - SThomas 8Jul22
    else:
        x_displacement[:,:] = np.asarray(displacement_strips[0])[np.newaxis,:]
    return x_displacement


def displacement_maker_y_2D(displacement_strips, strip_centres, x, y):
    """After the y-displacements have been made in 1D strips, this
    function is called to make it a full 2D displacement using 1D
    interpolation

    Returns
    -------
    2D numpy array the same shape as the frames

    """
    # creates empty displacement array the same shape as the frames
    y_displacement = np.zeros([len(y), len(x)])
    ### SThomas 8Jul2022 - error when the first strip is the same width
    # as one of the dimensions, so the interpolation fails. This if
    # statement has been brought in to fix that. Error initial in x,
    # this has been copied from displacment_x_maker_2D ###
    # if len(displacement_strips) > 1:    # proceed as normal, sthomas 8Jul22
    ### SThomas 10Mar2023 - len isn't a good way to check this, instead
    # changing to look at the specific dimension size
    if displacement_strips.shape[1] > 1:    # proceed as normal, sthomas 10Mar23
        # iterates over the number of y pixels
        for i in range(0, len(y)):
            # interpolates for all x positions at pixel y_i
            f = interpolate.interp1d(strip_centres, displacement_strips[i,:], \
            kind = 'linear', bounds_error = False, fill_value = \
            (displacement_strips[i,0], displacement_strips[i,-1]))
            y_displacement[i,:] = f(x)
    # new else statement - SThomas 8Jul22
    else:
        y_displacement[:,:] = np.asarray(displacement_strips[0])[:,np.newaxis]
    return y_displacement


def whole_x_iteration(x, y, xx, yy, oxx, oyy, z1, z2, strip_width, \
frame_bool, strip_bool, m, global_distance_new_bool, path_finder_new_bool, \
local_distance_new_bool, alpha, fast=True, l=3.): # SThomas added fast bool 14/aug/2020 # SThomas added l=3. kwarg 3/Feb/2021
    """Called at the start of the x iteration, calls all the relevant
    fucntions and performs one iteration

    Returns
    -------
    3 x 2D numpy arrays, each the same shape as the frames - the
    distorted frame, and the moved x and y positions

    """
    if local_distance_new_bool == True:
        displacement_strips, strip_centres = displacement_strip_maker_x_new(x, \
                            z1, z2, strip_width, frame_bool, strip_bool, m, \
                        global_distance_new_bool, path_finder_new_bool, alpha, \
                                                                    fast=fast, l=l) # SThomas added fast bool 14/aug/2020
    else:
        displacement_strips, strip_centres = displacement_strip_maker_x(x, z1, \
                                z2, strip_width, frame_bool, strip_bool, m, \
                                global_distance_new_bool, path_finder_new_bool,
                                                                    fast=fast, l=l) # SThomas added fast bool 14/aug/2020
    displacement_x_2D = displacement_maker_x_2D(displacement_strips, \
    strip_centres, x, y)
    distorted_xx = the_x_distorter(xx, displacement_x_2D)
    distorted_z1 = duplicate_x_finder_1D_interp(distorted_xx, z1, x)
    oxx_moved, oyy_moved = position_interper_x(distorted_xx, oxx, oyy, x)
    return distorted_z1, oxx_moved, oyy_moved


def whole_y_iteration(x, y, xx, yy, oxx, oyy, z1, z2, strip_width,
                frame_bool, strip_bool, m, global_distance_new_bool,
                path_finder_new_bool, local_distance_new_bool,
                alpha, fast=True, l=3.): # SThomas added fast bool 14/aug/2020 # SThomas added l=3. kwarg 3/Feb/2021
    """Called at the start of the y iteration, calls all the relevant
    fucntions and performs one iteration

    Returns
    -------
    3 x 2D numpy arrays, each the same shape as the frames - the
    distorted frame, and the moved x and y positions

    """
    if local_distance_new_bool == True:
        displacement_strips, strip_centres = displacement_strip_maker_y_new(y,
                            z1, z2, strip_width, frame_bool, strip_bool,
                            m, global_distance_new_bool, path_finder_new_bool,
                            alpha, fast=fast, l=l) # SThomas added fast bool 14/aug/2020
    else:
        displacement_strips, strip_centres = displacement_strip_maker_y(y, z1,
                            z2, strip_width, frame_bool, strip_bool, m,
                            global_distance_new_bool, path_finder_new_bool,
                            fast=fast, l=l) # SThomas added fast bool 14/aug/2020
    displacement_y_2D = displacement_maker_y_2D(displacement_strips,
    strip_centres, x, y)
    distorted_yy = the_y_distorter(yy, displacement_y_2D)
    distorted_z1 = duplicate_y_finder_1D_interp(distorted_yy, z1, y)
    oxx_moved, oyy_moved = position_interper_y(distorted_yy, oxx, oyy, y)
    return distorted_z1, oxx_moved, oyy_moved


def whole_x_and_y_iteration(x, y, xx, yy, oxx, oyy, z1, z2, strip_width,
                    frame_bool, strip_bool, m, global_distance_new_bool,
                    path_finder_new_bool, x_first, local_distance_new_bool,
                    alpha, fast=True, l=3.): # SThomas added l=3. kwarg 3/Feb/2021
    """Calls the x and y whole iterations of the algorithms.

    Returns
    -------
    3 x 2D numpy arrays, each the same shape as the frames - the
    distorted frame, and the moved x and y positions

    """

    if x_first:
        z1, oxx, oyy = whole_x_iteration(x, y, xx, yy, oxx, oyy, z1, z2,
                                strip_width, frame_bool, strip_bool, m,
                                global_distance_new_bool, path_finder_new_bool,
                                local_distance_new_bool, alpha, fast=fast, l=l) # SThomas added l=3. kwarg 3/Feb/2021
        z1, oxx, oyy = whole_y_iteration(x, y, xx, yy, oxx, oyy, z1, z2,
                                strip_width, frame_bool, strip_bool, m,
                                global_distance_new_bool, path_finder_new_bool,
                                local_distance_new_bool, alpha, fast=fast, l=l) # SThomas added l=3. kwarg 3/Feb/2021
    else:
        z1, oxx, oyy = whole_y_iteration(x, y, xx, yy, oxx, oyy, z1, z2,
                                strip_width, frame_bool, strip_bool, m,
                                global_distance_new_bool, path_finder_new_bool,
                                local_distance_new_bool, alpha, fast=fast, l=l) # SThomas added l=3. kwarg 3/Feb/2021
        z1, oxx, oyy = whole_x_iteration(x, y, xx, yy, oxx, oyy, z1, z2,
                                strip_width, frame_bool, strip_bool, m,
                                global_distance_new_bool, path_finder_new_bool,
                                local_distance_new_bool, alpha, fast=fast, l=l) # SThomas added l=3. kwarg 3/Feb/2021
    return z1, oxx, oyy


def setup_func(frame):
    """Set up the x and y pixel numbers in the algorithm"""
    y_len = frame.shape[0]
    x_len = frame.shape[1]
    x = np.linspace(1., x_len, x_len)
    y = np.linspace(1., y_len, y_len)
    xx, yy = np.meshgrid(x, y)
    return x, y, xx, yy


def all_iterations(z1, z2, oxx, oyy, strip_width_list, m_list, frame_bool=False,
                                strip_bool=True, global_distance_new_bool=True,
                                path_finder_new_bool=True, x_first=True,
                                local_distance_new_bool=True, alpha='cosine',
                                fast=True, l=3.): # SThomas added l=3. kwarg 3/Feb/2021
    """Main function to be called that operates the entire
    algorithm. See `DTW` for details of the inputs and outputs

    Returns 3 x 2D numpy arrays, each the same shape as the frames - the
    distorted frame, and the moved x and y positions. The distorted z1 frame is
    no longer required in the algorithm but is returned should the user wish to
    compare the transformation of the image with the one it was compared with
    """


    x, y, xx, yy = setup_func(z1)
    for i in range(0,len(strip_width_list)):
        z2, oxx, oyy = whole_x_and_y_iteration(x, y, xx, yy, oxx, oyy, z2, z1,
                        strip_width_list[i], frame_bool, strip_bool, m_list[i],
                        global_distance_new_bool, path_finder_new_bool, x_first,
                        local_distance_new_bool, alpha, fast=fast, l=l) # SThomas added l=3. kwarg 3/Feb/2021
    return z2, oxx, oyy


def displacement_maker(xx, yy, oxx, oyy):
    """Takes in the original x/R and y/z coordinates, as well as the
    distorted ones from the algorithm to compute the 2D velocity

    Returns 2 x 2D numpy arrays, the x velocity and y velocity, each
    with the same shape as the frames being compared. Note - almost
    certainly contains NaNs
    """
    x_displacement = oxx - xx
    y_displacement = oyy - yy
    return x_displacement, y_displacement


def DTW(z1, z2, oxx, oyy, strip_width_list=[32,22,16,12,8,6,4],
                                m_list=[11,8,6,4,3,2,2], frame_bool=False,
                                strip_bool=True, global_distance_new_bool=True,
                                path_finder_new_bool=True, x_first=True,
                                local_distance_new_bool=True, alpha='cosine',
                                fast=True, l=3.): # SThomas added l=3. kwarg 3/Feb/2021
    """Compare two frames, displaced in time, with each other

    This function should be called to run the majority of the algorithm

    Parameters
    ----------
    z1 : array_like
        First frame to be compared
    z2 : array_like
        Second frame to be comapred, later in time to frame 1
    oxx : array_like
        Original x/R coordinates, needed to calculate the correct
        velocity direction and magnitude. 2D array the same shape as
        ``z1`` and ``z2``.
    oyy : array_like
        Original y/Z coordinates, needed to calculate the correct
        velocity direction and magnitude. 2D array the same shape as
        ``z1`` and ``z2``.
    strip_width_list : array_like
        List/numpy array that informs the width of the strips to be
        used in the iterations of the algorithm.  Should be a 1D
        list/numpy arary.
    m_list : array_like or None
        List/numpy array that informs the number of elements from the
        ``I=J`` line that the algorithm should search. Should be a 1D
        list / numpy array the same length as ``strip_width_list``.
        ``m_list`` can also be ``None`` (or a list of ``None`` with
        the same length as ``strip_width_list``) if the whole
        `global_distance` matrix space is to be searched.
    frame_bool : bool
        If ``True``, normalise each frame to its maximum value
    strip_bool : bool
        If ``True``, normalise each strip to its maximum value
    global_distance_new_bool : bool
        If ``False`` use the old/original D matrix finding algorithm;
        otherwise use the new one which tries to move the warp path
        back to the ``i==j`` line when it can.
    path_finder_new_bool : bool
        If ``False`` use the old/original method of using
        `numpy.argmin` on the ``D_end`` variable; otherwise use the
        new one which will tell if there are mutliple minima and
        return the one closest to the ``i==j`` point on ``D_end``.
    x_first : bool
        If ``True``, iterates over the x-direction first, otherwise
        does the y-direction first.
    alpha : str
        If equal to ``"cosine"`` it uses a cosine as the window
        function in the orthogonal direction in the strips. If it
        equals anything else, then it doesn't use a window function at
        all.
    fast : bool
        If ``True`` uses a compiled Fortran implementation of key
        parts of the algorithm, otherwise uses a pure Python
        implementation
    l : float or None
        Used in the boxcar smoothing. If any consecutive displacement
        in a strip is greater than the window parameter divided by
        ``l`` (``displacement > (m / l)``) then smoothing is
        continuously applied. If ``l is None``, then the boxcar is not
        used at all.

    Returns
    -------
    vel_x : array_like
        velocity in the x direction - same shape as the input frames
    vel_y : array_like
        velocity in the y direction - same shape as the input frames
    """
    if m_list == None: # added SThomas 6/Aug/2020 to allow for no search window
        m_list = [None] * len(strip_width_list)
    z3, xx, yy = all_iterations(z1, z2, oxx, oyy, strip_width_list, m_list,
                    frame_bool=frame_bool, strip_bool=strip_bool,
                    global_distance_new_bool=global_distance_new_bool,
                    path_finder_new_bool=path_finder_new_bool, x_first=x_first,
                    local_distance_new_bool=local_distance_new_bool,
                    alpha=alpha, fast=fast, l=l) # SThomas added l=3. kwarg 3/Feb/2021
    vel_x, vel_y = displacement_maker(oxx, oyy, xx, yy)
    return vel_x, vel_y


def roundSF(number, SF=2):
    """Round to a specified number of significant figures"""
    N = -int(floor(np.log10(np.abs(number)))) + (SF - 1)
    return round(number, N)


def coordinates_interp(R, z, mult=20, preserveShape=False):
    """Refine coordinate arrays to higher resolution

    Parameters
    ----------
    R : array_like
        2D coordinate array
    z : array_like
        2D coordinate array
    mult : int
        Factor to increase resolution by
    preserveShape : bool
        If ``True``, preserve the non-rectangular shape of the BES
        view

    Returns
    -------
    R_1D, Z_1D, R_2D, Z_2D
        The new, higher resolution 1D coordinate arrays, along with
        the 2D versions as produced by `numpy.meshgrid`

    """

    myshape = R.shape
    # length of new arrays
    lenR = (myshape[1] - 1) * mult + 1
    lenz = (myshape[0] - 1) * mult + 1
    # 1D arrays
    Rin = np.linspace(R.min(), R.max(), lenR)
    ### Change, SThomas 25Jan2023
    ### zin was the wrong way round, swapped max with min
    zin = np.linspace(z.max(), z.min(), lenz)
    # to preserve the tapered shape of the BES view or not
    if preserveShape:
        # empty arrays
        Rinterp = np.zeros((lenz, lenR))
        zinterp = np.zeros((lenz, lenR))
        # create limit for top of the images
        mtop = (z[0,-1] - z[0,0]) / (R[0,-1] - R[0,0])
        ctop = z[0,0] - mtop * R[0,0]
        # create limite for the bottom of the images
        mbot = (z[-1,-1] - z[-1,0]) / (R[-1,-1] - R[-1,0])
        cbot = z[-1,0] - mbot * R[-1,0]
        # make the ararys
        # all rows at once, as R doesn't change
        Rinterp[:,:] = Rin
        # iteratoe over the R positions for z
        for i in range(0, lenR):
            zinterp[:,i] = np.linspace(Rin[i] * mtop + ctop,
                                       Rin[i] * mbot + cbot, lenz)
    else:
        # 2D meshgrid arrays
        Rinterp, zinterp = np.meshgrid(Rin, zin)
    return Rin, zin, Rinterp, zinterp


def frame_interp(frame, Rz, Rinterp, zinterp, method='cubic'):
    """Interpolate BES data higher resolution grid

    Parameters
    ----------
    frame : array_like
        BES data
    Rz : array_like
        Original coordinates as ``(n, 2)`` array
    Rinterp : array_like
        New R coordinates as 1D array
    zinterp : array_like
        New Z coordinates as 1D array
    method : str
        Interpolation method

    Returns
    -------
    array_like
        Interpolated BES data

    """

    frame2 = interpolate.griddata(Rz, frame.flatten(),
             (Rinterp, zinterp), method=method)
    boo = np.where(np.isnan(frame2))
    frame2[boo] = interpolate.griddata(Rz, frame.flatten(),
                  (Rinterp[boo], zinterp[boo]), method='nearest')
    return frame2


def velocity_downsample(velx, vely, Rinterp, zinterp, R, z):
    """
    Velocities should be in shape ``(len(zinterp), len(Rinterp))`` and
    be only one frame long. R/zinterp should be the larger size and
    R/z should be the original 8x8/4x8 BES size.  For coordinates,
    should be been transposed so that R[0,0] is Rmin and z[0,0] is
    zmin, with the first axis increasing z and the second axis
    increasing R
    """
    # empty arrays for the vx as we go
    vx = [[[], [], [], [], [], [], [], []],
          [[], [], [], [], [], [], [], []],
          [[], [], [], [], [], [], [], []],
          [[], [], [], [], [], [], [], []],
          [[], [], [], [], [], [], [], []],
          [[], [], [], [], [], [], [], []],
          [[], [], [], [], [], [], [], []],
          [[], [], [], [], [], [], [], []]]
    vy = [[[], [], [], [], [], [], [], []],
          [[], [], [], [], [], [], [], []],
          [[], [], [], [], [], [], [], []],
          [[], [], [], [], [], [], [], []],
          [[], [], [], [], [], [], [], []],
          [[], [], [], [], [], [], [], []],
          [[], [], [], [], [], [], [], []],
          [[], [], [], [], [], [], [], []]]
    ### change SThomas 25Jan2023
    ### top and bottom needed switching since I'd changed
    ### something with the coordinates
    ### change SThomas 27Feb2023
    ### needs altering again, and needs to check where
    ### the top and bottom are
    if z[0].mean() < 0:
        I = -1
        J = 0
    else:
        I = 0
        J = -1
    # create limit for top of the images
    mtop = (z[I,-1] - z[I,0]) / (R[I,-1] - R[I,0])
    ctop = z[I,0] - mtop * R[I,0]
    # create limite for the bottom of the images
    mbot = (z[J,-1] - z[J,0]) / (R[J,-1] - R[J,0])
    cbot = z[J,0] - mbot * R[J,0]
    # boolean to do or not
    boo = (zinterp <= mtop * Rinterp + ctop) * \
          (zinterp >= mbot * Rinterp + cbot)
    # iterate over rows
    for i in range(0, Rinterp.shape[0]):
        for j in range(0, Rinterp.shape[1]):
            if boo[i,j]:
                # distance from interp location to original views
                D = np.sqrt((R - Rinterp[i,j])**2 + (z - zinterp[i,j])**2)
                Dmin = np.where(D == D.min())
                # append accordingly
                vx[Dmin[0][0]][Dmin[1][0]].append(velx[i,j])
                vy[Dmin[0][0]][Dmin[1][0]].append(vely[i,j])
    # empty arrays for final velocities and variances
    velX = np.zeros_like(R)
    velY = np.zeros_like(R)
    stdX = np.zeros_like(R)
    stdY = np.zeros_like(R)
    # iterate over the original sizes
    for i in range(0, R.shape[0]):
        for j in range(0, R.shape[1]):
            velX[i,j] = np.nanmean(vx[i][j])
            velY[i,j] = np.nanmean(vy[i][j])
            stdX[i,j] = np.nanstd(vx[i][j])
            stdY[i,j] = np.nanstd(vy[i][j])
    return velX, velY, stdX, stdY
