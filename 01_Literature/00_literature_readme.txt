S Thomas 9thSept2019
________________________________________________________________________________________________________

This is a quick readme for the documentation supplied for DTW.

To begin with, one should start with sakoe_1978, as I believe this is the first paper that uses the 1D ODP/DTW algorithm for speech recognition. It may be somewhat redundant with later papers but for completeness is still a good starting point.

Next are the four papers by Quenot in chronological order. LThis first paper from 1992 introduces the first use of the 2D DTW algorithm for velocimetry and gives some preliminary results. 1996 introduces a few 'improvements' on the 1992 paper, and 1998 introduces even more in what I consider the best paper of the four from Quenot.

- If you were to only read one of the papers here, then Quenot 1998 should be the one you read.

Quenot 2000 introduces the idea of 2 cameras looking at one location to project onto a 2D plane. This one is not used at all by me but is included just for completeness.

Kriete 2018 is a paper that specifically uses the DTW algorithm for fusion plasmas, specifically in zonal flows from BES diagnostics. I have included this paper as it is somewhat useful to see it in context, but also I think the test that they introduce is a good one, and one that I have preliminarily implemented myself.

Other papers are available, and if the user finds any that they think are useful should be added to this list.
________________________________________________________________________________________________________
