#
# Makefile
# Edward Higgins, 2020-05-20 17:29
#

all: dtw_f

dtw_f: dtw_f.cpython-38-x86_64-linux-gnu.so

dtw_f.cpython-38-x86_64-linux-gnu.so: dtw/dtw_f.f90
	f2py -c -m dtw_f $^

clean:
	rm -f dtw_f.cpython-38-x86_64-linux-gnu.so
